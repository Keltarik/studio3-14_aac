/**
 * Created by zachot on 02.08.15.
 */
(function () {
  'use strict';

  var empty = function () {
  };
  /**
   *
   * @param {string} url
   * @param {FormData} data
   * @param {Function} load
   * @param {Function=} error
   * @param {Function=} process
   */
  window.ajax = function (url, data, load, error, process) {

    data.append('_', Date.now());
    data.append('_csrf', $('input[name="_csrf"]').val());


    error = (typeof error === "undefined") ? empty : error;
    process = (typeof process === "undefined") ? empty : process;

    var ajax = new XMLHttpRequest();
    ajax.cache = false;

    ajax.upload.addEventListener("progress", function (progress) {
      process(progress.loaded / progress.total);
    }, false);

    ajax.addEventListener("load", function (response) {
      load(JSON.parse(response.target.response));
    }, false);

    ajax.onreadystatechange = function (oEvent) {
      if (ajax.readyState === 4) {
        if (ajax.status !== 200) {
          alert('Error file upload, try another file');
          error();
        }
      }
    };

    ajax.open("POST", url);
    ajax.send(data);

  };


  $(document).on('change', '.upload-file', function (event) {
    var file = event.target.files[0];
    var parent = $(event.target).parents('.form-group').first();
    var hidden = parent.find('.hidden-attachment').first();

    var img = parent.find('.preview img')[0];
    //var reader = new FileReader();
    //
    //reader.onload = function (event) {
    //  img.src = event.target.result;
    //};
    //
    //reader.readAsDataURL(file);

    parent.find('.preview').show().removeClass('hide');
    //parent.find('.input').hide().toggleClass('hide');

    var data = new FormData();
    data.append(event.target.name, file);
    data.append('name', event.target.getAttribute('data-name'));

    ajax('/admin/json/upload', data, function (response) {
      //console.log(response);
      if (response.success && !response.error) {
        hidden.val(response.file.id);
        img.src = response.file.url;
      }
    })
  });

  var team = {}, options = ''
    , att = {};

  var partners = {}, partners_options = '',
      partners_att = {};

  function loadTeam() {
    $.ajax('/admin/json/team')
      .done(function (response) {
        for (var i = 0; i < response.length; i++) {
          team[response[i].id] = response[i];
          options += '<option value="' + response[i].id + '">' + response[i].name + '</option>'
        }
      });
  }

  function loadPartners() {
    $.ajax('/admin/json/partner')
      .done(function (response) {
        for (var i = 0; i < response.length; i++) {
            partners[response[i].id] = response[i];
            partners_options += '<option value="' + response[i].id + '">' + response[i].name + '</option>'
        }
      });
  }

  loadTeam();
  loadPartners();

  function getUrl(id, img) {
    if (att[id]) {
      img.src = att[id];
      return att[id];
    }
    $.ajax('/admin/json/image-url?id=' + id)
      .done(function (response) {
        att[id] = response.url;
        img.src = att[id];
      });
    }

    function getPartnerUrl(id, img) {
        if (partners_att[id]) {
            img.src = partners_att[id];
            return partners_att[id];
        }
        $.ajax('/admin/json/image-url?id=' + id)
            .done(function (response) {
                partners_att[id] = response.url;
                img.src = partners_att[id];
            });
    }

  $(document).on('click', '.btn-add-partner', function (event) {
    event.preventDefault();
    event.stopPropagation();

    var jThis = $(this)
        , jBody = $(jThis.attr('href'));

    jBody.append('<tr><td style="vertical-align: middle;"><button class="btn btn-sm btn-danger btn-partner-delete">Delete</button>' +
        '</td><td><div class="row"><div class="col-xs-4" style="width: 100px;"><img src="" alt="" class="img-responsive"></div><div class="col-xs-8"><div class="form-group field-editproject-partner"><label class="control-label" for="editproject-partner"></label><select id="editproject-partner" class="form-control change-partner" name="EditProject[partner_ids][]">'
        + partners_options +
        '</select></div></div></div></td></tr>');

  });

  $(document).on('change', '.change-partner', function (event) {
    var partner = partners[event.target.value];
    var img = $(this).parents('.row').first().find("img")[0];
      getPartnerUrl(partner.logo, img);
  });

  $(document).on('change', '.change-person', function (event) {
    var person = team[event.target.value];
    var img = $(this).parents('.row').first().find("img")[0];

    getUrl(person.photo, img);
  });

  $(document).on('click', '.btn-add-person', function (event) {
    event.preventDefault();
    event.stopPropagation();

    var jThis = $(this)
      , jBody = $(jThis.attr('href'));


    jBody.append('<tr><td style="vertical-align: middle;">' +
      '<button class="btn btn-sm btn-person-up">Up</button><button class="btn btn-sm btn-info btn-person-down">Down</button><button class="btn btn-sm btn-danger btn-person-down">Delete</button>' +
      '</td><td><div class="row"><div class="col-xs-4" style="width: 100px;"><img src="" alt="" class="img-responsive"></div><div class="col-xs-8"><div class="form-group field-editproject-team"><label class="control-label" for="editproject-team"></label><select id="editproject-team" class="form-control change-person" name="EditProject[team][]">'
      + options +
      '</select><div class="help-block"></div></div></div></div></td><td><div class="form-group field-editproject-role"><label class="control-label" for="editproject-role"></label><input type="text" id="editproject-role" class="form-control" name="EditProject[role][]" maxlength="255" placeholder="Роль"><div class="help-block"></div></div></td></tr>');

  });
  /**
   *
   <button class="btn btn-sm btn-person-up">Up</button>
   <button class="btn btn-sm btn-info btn-person-down">Down</button>
   <button class="btn btn-sm btn-danger btn-person-down">Delete</button>
   */
  $(document).on('click', '.btn-person-up', function (event) {
    event.preventDefault();
    event.stopPropagation();

    var jTr = $(this).parents('tr').first()
      , jTBody = jTr.parent();

    var jTrs = jTBody.find('tr');
    var t = false;
    jTrs.each(function (index, node) {
      if (index == 0 || t)
        return;
      if ($(node).is(jTr)) {
        jTr.remove();
        $(jTrs[index - 1]).before(node);
        t = true;
      }
    })
  });
  $(document).on('click', '.btn-person-down', function (event) {
    event.preventDefault();
    event.stopPropagation();

    var jTr = $(this).parents('tr').first()
      , jTBody = jTr.parent();

    var jTrs = jTBody.find('tr');
    var t = false;
    jTrs.each(function (index, node) {
      if (index == jTrs.length - 1 || t)
        return;
      if ($(node).is(jTr)) {
        jTr.remove();
        $(jTrs[index + 1]).after(node);
        t = true;
      }
    })
  });
  $(document).on('click', '.btn-person-delete', function (event) {
    event.preventDefault();
    event.stopPropagation();

    var jTr = $(this).parents('tr').first().remove();
  });

  $(document).on('click', '.btn-partner-delete', function (event) {
    event.preventDefault();
    event.stopPropagation();

    var jTr = $(this).parents('tr').first().remove();
  });

  $(document).ready(function () {


    $(document).magnificPopup({
      delegate : 'a.add-room,a.edit-room',
      type : 'ajax',
      alignTop : true,
      overflowY : 'scroll',
      callbacks : {
        parseAjax : function (mfpResponse) {
          mfpResponse.data = mfpResponse.data.html;
        }
      }
    });
  });
  var close = false;
  window.saveAndClose = function () {
    close = true;
  };

  $(document).on('submit', '.form-room', function (event) {

    event.stopPropagation();
    event.preventDefault();

    var data = $(event.target).serialize()
      , dataArray = $(event.target).serializeArray();

    $.ajax('/admin/json/add-room', {
      method : 'post', data : data
    }).done(function (response) {
      $('#room_inner').html(response.html);
      if (!response.id)
        return;
      var jRoom = $('#room_' + response.id);

      if (jRoom.length > 0) {
        jRoom.find('h4').html(response.title);
      } else {
        $('#project_rooms').append('<tr><td style="vertical-align: middle;"><button class="btn btn-sm btn-person-up">Up</button><button class="btn btn-sm btn-info btn-person-down">Down</button><button class="btn btn-sm btn-danger btn-person-delete">Delete</button> </td><td><input type="hidden" name="EditProject[room_ids][]" value="' + response.id + '"><h4>' + response.title + '</h4><a href="/admin/json/add-room?id=' + response.id + '" class="edit-room">Редактировать</a></td></tr>');
      }

      if (close) {
        close = false;
        $.magnificPopup.instance.close();
      }

    })
  });

  $(document).on('submit', '.form-review', function (event) {

    event.stopPropagation();
    event.preventDefault();

    $.ajax('/admin/json/add-review', {
      method : 'post', data : $(event.target).serialize()
    }).done(function (response) {
      $('#room_review').html(response.html);
      if (!response.id)
        return;

      var jReview = $('#review_' + response.id);

      if (jReview.length > 0) {

        jReview.find('h4').html(response.author_name);
        jReview.find('div.pr').html(response.author_profession);

        jReview.find('img').attr('src', response.photo_url);
        jReview.find('div.review_text').html(response.text);

      } else {
        $('#project_reviews').append('<tr id="review_' + response.id + '"><td style="vertical-align: middle;"><button class="btn btn-sm btn-person-up">Up</button><button class="btn btn-sm btn-info btn-person-down">Down</button><button class="btn btn-sm btn-danger btn-person-delete">Delete</button></td><td style="width: 300px;"><input type="hidden" name="EditProject[review_ids][]" value="' + response.id + '"><div class="row"><div class="col-xs-4"><img src="' + response.photo_url + '" height="50" class="img-responsive" alt=""></div><div class="col-xs-8"><h4>' + response.author_name + '</h4><div class="pr">' + response.author_profession + '</div></div></div><a href="/admin/json/add-review?id=' + response.id + '" class="edit-room">Редактировать</a></td><td style="font-style: italic; font-size: 10px;"><div class="review_text">' + response.text + '</div></tr>');
      }

      if (close) {
        close = false;
        $.magnificPopup.instance.close();
      }
    })
  });


  $(document).on('click', '.js-toggle', function (event) {
    event.preventDefault();
    event.stopPropagation();

    var _this = this;
    var to = this.getAttribute('data-toggle-inner')
      , from = this.innerHTML;

    $.ajax(this.getAttribute('href'), {})
      .done(function (response) {

        if (!response.error) {

          _this.setAttribute('data-toggle-inner', from);
          _this.innerHTML = to;
        }
      });

  });


  $(document).on('click', '.js-delete', function (event) {
    event.preventDefault();
    event.stopPropagation();

    var _this = this;
    var toDelete = this.getAttribute('data-to-delete')
      , deleteNode = $(this).parents(toDelete).first();

    var table = $(this).parents('table').first();

    var i = null;

    table.find('tr[role="row"]').each(function (index, node) {
      if ($(node).is(deleteNode))
        i = index - 1;
    });

    if (confirm('Вы уверены что хотите удалить запись?')) {
      $.ajax(this.getAttribute('href'), {})
        .done(function (response) {
          if (!response.error) {
            $(table).dataTable().fnDeleteRow(i);
            window.location = window.location;
          }
        });
    }


  });
}());