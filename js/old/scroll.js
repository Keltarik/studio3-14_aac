(function () {
  'use strict';
  var header = $('.header');
  var jDocument = $(document);
  var jPageMenu = $('.site-page-menu');

  //var jNav = header.find('.header-nav');

  var fixedHeader;

  var winH = window.innerHeight;

  $(window).resize(function () {
    winH = window.innerHeight;
  });

  var changeColor = false;
  jDocument.on('scroll', function (event) {
    var jScroll = jDocument.scrollTop();
    if (jScroll >= winH - 80 && jScroll < winH - 51) {
      if (typeof fixedHeader !== "undefined" || !fixedHeader) {
        header.addClass('header__static');
      }

      header.css('height', winH - jScroll);
      //.css('padding-top', ( winH - jScroll - jNav.height()) / 2);
      fixedHeader = true;
    } else if (jScroll >= winH - 51) {
      if (typeof fixedHeader !== "undefined" || !fixedHeader) {
        header.addClass('header__static');
      }
      header.css('height', '').css('padding-top', '');
      fixedHeader = true;
    } else {
      if (typeof fixedHeader !== "undefined" || fixedHeader) {
        header.removeClass('header__static');
      }
      header.css('height', '').css('padding-top', '');
      fixedHeader = false;
    }


    if (jScroll > 5) {
      if (!changeColor) {
        header.css('background-color', '#e6e6e6');
      }
      changeColor = true;
    } else {
      if (changeColor) {
        header.css('background-color', '');
      }
      changeColor = false;
    }

    if (typeof stopSlider !== "undefined") {
      if (jScroll >= winH / 2)
        stopSlider();
      else
        startSlider();
    }

    if (jScroll > 600 && jPageMenu.length > 0) {
      jPageMenu.addClass('site-page-menu__show');
    } else if (jPageMenu.length > 0) {
      jPageMenu.removeClass('site-page-menu__show');
    }


    if (menuOPT.length > 0 && jPageMenu.length > 0) {
      var i = 0;
      while (jScroll > menuOPT[i]) {
        i++;
      }

      jPageMenu.find('a').each(function (index, node) {
        if (index == i - 1)
          node.classList.add('active');
        else
          node.classList.remove('active');
      });
    }
  });

  var menuOPT = [];

  if (jPageMenu.length > 0) {
    loadOffset();
    setInterval(loadOffset, 100);
  }

  function loadOffset() {
    menuOPT = [];
    jPageMenu.find('a').each(function (index, node) {
      var jTitle = $(node.getAttribute("href"));
      menuOPT.push(jTitle.offset().top - 152);
    });
  }

  var body = $("html, body");

  jDocument.on('click', '.js-scroll-link', function (event) {
    event.stopPropagation();
    event.preventDefault();
    var hash = this.getAttribute('href')
      , node = $(hash);

    body.animate({
      scrollTop : node.offset().top - 100
    }, 300, function () {
      window.location.hash = hash;
    });
  });
}());