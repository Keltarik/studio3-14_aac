/*
WebFont.load({
    google: {
        families: ['Ubuntu:300,700:cyrillic']
    }
});
*/
$(document).ready(function(){

    //Anchor
    $('a[href*=#]').bind("click", function(e){
        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top
        }, 1000);
        e.preventDefault();
    });


    // Add inactive class to all steps except the first
    $(".form-step").slice(1).addClass("form-step-inactive");

    var step_count = $(".step-by-step-form .form-step").length;
    $(".steps__gift-length-total b").append(step_count);
    var step_now = 1;

    // Next Click
    $('.step-form-next').on('click', function(){
        $(this).closest('.form-step').next(".form-step").removeClass("form-step-inactive");
        $(this).closest('.form-step').addClass("form-step-inactive");
        step_now++;
        $(".steps__gift-length-now b").html(step_now);
    })

    // Prev Click
    $('.step-form-prev').on('click', function(){
        $(this).closest('.form-step').prev(".form-step").removeClass("form-step-inactive");
        $(this).closest('.form-step').addClass("form-step-inactive");
        step_now--;
        $(".steps__gift-length-now b").html(step_now);
    })


});

var secondsRemaining;
var intervalHandle;
function tick(){
    // grab the h1
    var timeDisplay =  document.getElementById("time");

    // turn the seconds into mm:ss
    var min = Math.floor(secondsRemaining / 60);
    var sec = secondsRemaining - (min * 60);

    //add a leading zero (as a string value) if seconds less than 10
    if (sec < 10) {
        sec = "0" + sec;
    }

    // concatenate with colon
    var message = "00:0" + min.toString() + ":" + sec;

    // now change the display
    timeDisplay.innerHTML = message;

    // stop is down to zero
    if (secondsRemaining === 0){
        // alert("Done!");
        clearInterval(intervalHandle);
        resetPage();
    }
    secondsRemaining--;
}
var minutes = 3;
function startCountdown(){
    secondsRemaining = minutes * 60;
    intervalHandle = setInterval(tick, 1000);
}
var star_time_click = 1;
if(star_time_click == "1") {
    $(".star_time").click(function() {
        star_time_click++;
        startCountdown();
    })
}



(function($){
    $(window).on("load",function(){
        $(".mcs-horizontal-example").mCustomScrollbar({
            axis:"x",
            theme:"dark-3",
            advanced:{
                autoExpandHorizontalScroll:true //optional (remove or set to false for non-dynamic/static elements)
            }
        });
    });
})(jQuery);

$(function() {
    var selectedClass = "";
    $(".project__nav li a").click(function(){
        $(".project__nav li a").removeClass('active');
        $(this).addClass('active');
        selectedClass = $(this).attr("data-rel");
        $("#portfolio").fadeTo(10, 0.1);
        $("#portfolio div").not("."+selectedClass).addClass('remove');
        setTimeout(function() {
            $("."+selectedClass).removeClass('remove');
            $("#portfolio").fadeTo(500, 1);
        }, 500);

        $("#portfolio div.all").removeClass('project__info-small');
        $("#portfolio div.all").removeClass('project__info-more');
        $("#portfolio div.all").removeClass('dn');
        var i = 0;
        $('#portfolio .'+selectedClass).each(function() {
            i++;
            if (i > 4) {
                $(this).addClass('project__info-small');
            }
            if (i > 7) {
                $(this).addClass('project__info-more');
            }
            if (i > 10) {
                $(this).addClass('dn');
            }
        });
    });
    $(".show_more").click(function () {
        var count = $("#portfolio .project__info-more").length;
        count = count - 7;

        // $("#portfolio .project__info").each(function(){
        //     if(count > 3){
        //         $(this).addClass('someClass');
        //     }
        // });
        if (count > 3) {
            $("#portfolio .project__info").slice(2, 5).addClass("red");
        } else {
            console.log("count < 3");
        }
        console.log(count);
    });

    $('.open-modal').click(function() {
        $('.modal-wrapper').addClass('open');
        return false;
    });
    $('.close-modal').click(function () {
        $('.modal-wrapper').removeClass('open');
        return false;
    });
    $(document).mouseup(function (e){
        var div = $(".modal");
        if (!div.is(e.target)
            && div.has(e.target).length === 0) {
            $(".modal-wrapper").removeClass("open");
        }
    });
});

$(".owl-carousel").owlCarousel({
    margin:10,
    loop:true,
    dots: true,
    animateOut: 'fadeOut',
    nav: true,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    items: 1,
    navText : ["<i class='icon'><svg><use xlink:href='#icon_left'></use></svg></i>","<i class='icon'><svg><use xlink:href='#icon_right'></use></svg></i>"]
});


var $fileInput = $('.file-input');
var $droparea = $('.file-drop-area');

// highlight drag area
$fileInput.on('dragenter focus click', function() {
    $droparea.addClass('is-active');
});

// back to normal state
$fileInput.on('dragleave blur drop', function() {
    $droparea.removeClass('is-active');
});

// change inner text
$fileInput.on('change', function() {
    var filesCount = $(this)[0].files.length;
    var $textContainer = $(this).prev('.js-set-number');

    if (filesCount === 1) {
        // if single file then show file name
        $textContainer.text($(this).val().split('\\').pop());
    } else {
        // otherwise show number of files
        $textContainer.text(filesCount + ' files selected');
    }
});
