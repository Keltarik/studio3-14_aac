<?php
/**
 * Created by PhpStorm.
 * User: zachot
 * Date: 02.08.15
 * Time: 22:42
 */

namespace backend\controllers;


use backend\models\edit\EditReview;
use backend\models\edit\EditRoom;
use backend\models\edit\EditTeammate;
use backend\models\helper\Upload;

use backend\models\type\Attachment;
use backend\models\type\Project;
use backend\models\type\Teammate;
use backend\models\type\Partner;
use backend\models\type\Slider;

use common\models\type\Tag;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class JsonController extends Controller {
	public function beforeAction($action) {
		if (!parent::beforeAction($action))
			return false;

		Yii::$app->response->format = Response::FORMAT_JSON;
		return true;
	}

	function actionUpload() {
		/**
		 * @var $request
		 */
		$request = Yii::$app->request;

		$upload = (new Upload())->saveImageToUrl($request->post('name'));
		if ($upload->hasErrors())
			return ['error' => true,
				'success' => true,
				'errors' => $upload->errors];
		$att = Attachment::create($upload->getSrc());
		if ($att->hasErrors())
			return ['error' => true,
				'success' => true,
				'errors' => $att->errors];

		return [
			'error' => false,
			'success' => true,
			'file' => [
				'id' => $att->getAttribute('id'),
				'url' => $att->getUrl()
			]
		];
	}


	function actionTeam() {
		return Teammate::find()->all();
	}

    function actionPartner() {
        return Partner::find()->all();
    }


	function actionImageUrl() {
		$id = Yii::$app->request->get('id');
		return ['url' => Attachment::findOne(['id' => $id])->getUrl()];
	}

	function actionTogglePersonMain() {
		$id = Yii::$app->request->get('id');
		/**
		 * @var $person null|Teammate
		 */
		$person = Teammate::findOne(['id' => $id]);
		if (!$person)
			return [
				'error' => true, "success" => true
			];

		$person->setAttribute('main',
			(($person->getAttribute('main')) ? 0 : 1)
		);
		$person->save();
		return [
			'error' => false, "success" => true
		];
	}
	
	function actionTogglePersonPast() {
		$id = Yii::$app->request->get('id');
		/**
		 * @var $person null|Teammate
		 */
		$person = Teammate::findOne(['id' => $id]);
		if (!$person)
			return [
					'error' => true, "success" => true
			];
	
			$person->setAttribute('past',
					(($person->getAttribute('past')) ? 0 : 1)
					);
			$person->save();
			return [
					'error' => false, "success" => true
			];
	}

    function actionTogglePartnerVisible() {
        $id = Yii::$app->request->get('id');
        /**
         * @var $partner null|Partner
         */
        $partner = Partner::findOne(['id' => $id]);
        if (!$partner)
            return [
                'error' => true, "success" => true
            ];

        $partner->setAttribute(
            'visible',
            (($partner->getAttribute('visible')) ? 0 : 1)
        );
        $partner->save();
        return [
            'error' => false, "success" => true
        ];
    }
    
    function actionMoveSliderUp() {
    	$id = Yii::$app->request->get('id');
    	$p = Yii::$app->request->get('pos');
    	/**
    	 * @var $slide null|Slider
    	 */
    	$slide = Slider::findOne(['id' => $id]);
    	$slideUp = Slider::findOne(['position' => ($p - 1)]);
    	if (!$slide)
    		return [
    				'error' => true, "success" => true
    		];
    
    		$slide->setAttribute(
    				'position',
    				($slide->getAttribute('position') - 1)
    				);
    		$slide->save();
    		$slideUp->setAttribute('position', $p);
    		$slideUp->save();
    		return $this->redirect('@web/admin/slider', 302);
    }
    
    function actionMoveSliderDown() {
    	$id = Yii::$app->request->get('id');
    	$p = Yii::$app->request->get('pos');
    	/**
    	 * @var $slide null|Slider
    	 */
    	$slide = Slider::findOne(['id' => $id]);
    	$slideDown = Slider::findOne(['position' => ($p + 1)]);
    	if (!$slide)
    		return [
    				'error' => true, "success" => true
    		];
    
    		$slide->setAttribute(
    				'position',
    				($slide->getAttribute('position') + 1)
    				);
    		$slide->save();
    		$slideDown->setAttribute('position',$p);
    		$slideDown->save();
    		return $this->redirect('@web/admin/slider', 302);
    }
    
    function actionMoveTeammateUp() {
    	$id = Yii::$app->request->get('id');
    	$p = Yii::$app->request->get('pos');
    	/**
    	 * @var $person null|Teammate
    	 */
    	$person = Teammate::findOne(['id' => $id]);
    	$personUp = Teammate::findOne(['position' => ($p - 1)]);
    	if (!$person)
    		return [
    				'error' => true, "success" => true
    		];
    
    		$person->setAttribute(
    				'position',
    				($person->getAttribute('position') - 1)
    				);
    		$person->save();
    		$personUp->setAttribute('position', $p);
    		$personUp->save();
    		return $this->redirect('@web/admin/team', 302);
    }
    
    function actionMoveTeammateDown() {
    	$id = Yii::$app->request->get('id');
    	$p = Yii::$app->request->get('pos');
    	/**
    	 * @var $person null|Teammate
    	 */
    	$person = Teammate::findOne(['id' => $id]);
    	$personDown = Teammate::findOne(['position' => ($p + 1)]);
    	if (!$person)
    		return [
    				'error' => true, "success" => true
    		];
    
    		$person->setAttribute(
    				'position',
    				($person->getAttribute('position') + 1)
    				);
    		$person->save();
    		$personDown->setAttribute('position',$p);
    		$personDown->save();
    		return $this->redirect('@web/admin/team', 302);
    }

	function actionTogglePersonDelete() {
		$id = Yii::$app->request->get('id');
		/**
		 * @var $person null|Teammate
		 */
		$person = Teammate::findOne(['id' => $id]);
		if (!$person)
			return [
				'error' => true, "success" => true
			];

		$person->delete();
		return [
			'error' => false, "success" => true
		];
	}

	function actionToggleProjectDelete() {
		$id = Yii::$app->request->get('id');
		/**
		 * @var $project null|Project
		 */
		$project = Project::findOne(['id' => $id]);
		if (!$project)
			return [
				'error' => true, "success" => true
			];

		$project->delete();
		return [
			'error' => false, "success" => true
		];
	}

	function actionAddRoom() {

		$id = \Yii::$app->request->post('id', \Yii::$app->request->get('id'));
		$model = new EditRoom();
		if ($id && is_numeric($id))
			$model->loadRoom($id);

		if (\Yii::$app->request->isPost)
			if (!$model->go()) {
				if ($model->hasErrors('empty')) {
					$model->clearErrors('empty');
				}
			}

		return [
			'errors' => $model->hasErrors(),
			'success' => true,
			'id' => $model->id,
			'title' => $model->title,
			'html' => $this->renderFile('@app/views/edit/popup-room.php', ['model' => $model])
		];
	}

	function actionAddReview() {

		$id = \Yii::$app->request->post('id', \Yii::$app->request->get('id'));
		$model = new EditReview();
		if ($id && is_numeric($id))
			$model->loadReview($id);

		if (\Yii::$app->request->isPost)
			if (!$model->go()) {
				if ($model->hasErrors('empty')) {
					$model->clearErrors('empty');
				}
			}
		/**
		 * @var $_att Attachment
		 */
		$_att = Attachment::findOne(['id' => $model->author_photo]);

		return [

			'errors' => $model->hasErrors(),
			'success' => true,
			'id' => $model->id,

			'author_name' => $model->author_name,
			'author_profession' => $model->author_profession,
			'author_photo' => $model->author_photo,
			'photo_url' => ($_att) ? $_att->getUrl() : "",

			'text' => $model->text,

			'html' => $this->renderFile('@app/views/edit/popup-review.php', ['model' => $model])
		];
	}


	function actionAddTeammate() {

		$id = \Yii::$app->request->post('id', \Yii::$app->request->get('id'));
		$model = new EditTeammate();
		if ($id && is_numeric($id))
			$model->loadTeammate($id);

		if (\Yii::$app->request->isPost)
			if (!$model->go()) {
				if ($model->hasErrors('empty')) {
					$model->clearErrors('empty');
				}
			}

		return [
			'errors' => $model->hasErrors(),
			'success' => true,

			'html' => $this->renderFile('@app/views/edit/popup-room.php', ['model' => $model])
		];
	}

	/**
	 * @return array|\yii\db\ActiveRecord[]
	 */
	function actionPortfolio() {
//		return Portfolio::find()->all();

        try {
            $portfolio = Yii::$app->request->post();

            $pr = json_encode($portfolio['data']);
            if (file_put_contents(__DIR__ . "/../../../settings/portfolio.json", $pr)) {

            }

            return $portfolio;
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }


	}

	function actionGetPortfolio() {
		$pr = file_get_contents(__DIR__ . "/../../../settings/portfolio.json");
		return json_decode($pr);
	}

	/**
	 * @return array|\yii\db\ActiveRecord[]
	 */
	function actionProjects() {
		return Project::find()->all();
	}

    function actionTags() {
        return Tag::find()->all();
    }


}