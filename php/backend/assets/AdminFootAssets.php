<?php
/**
 * Created by PhpStorm.
 * User: zachot
 * Date: 02.08.15
 * Time: 0:23
 */

namespace backend\assets;


use yii\web\AssetBundle;

class AdminFootAssets extends AssetBundle {
	public $basePath = '@webroot';
	public $baseUrl = '@web';

	public $jsOptions = ['position' => \yii\web\View::POS_END];

	public $css = [
		'css/magnific-popup.css'
	];

	public $js = [
		'assets/bootstrap/js/bootstrap.min.js',

		"assets/metisMenu/dist/metisMenu.min.js",
		'assets/datatables/media/js/jquery.dataTables.min.js',
		'assets/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js',

		'js/jquery.magnific-popup.min.js',
		'js/sb-admin-2.js',
		'js/admin.js',
		'js/tinymce.upload.js',
	];

	public $depends = [
		'yii\web\JqueryAsset'
	];
}