<?php
/**
 * Created by PhpStorm.
 * User: zachot
 * Date: 02.08.15
 * Time: 0:15
 */

namespace backend\assets;


use yii\web\AssetBundle;

class AdminAssets extends AssetBundle {

	public $basePath = '@webroot';
	public $baseUrl = '@web';

	public $jsOptions = ['position' => \yii\web\View::POS_HEAD];

	public $css = [

		'assets/bootstrap/css/bootstrap.min.css',
		'assets/font-awesome/css/font-awesome.min.css',
		'assets/metisMenu/dist/metisMenu.min.css',


		'assets/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css',
		'assets/datatables-responsive/css/dataTables.responsive.css',


		'css/timeline.css',
		'css/sb-admin-2.css'
	];

	public $js = [

	];

	public $depends = [];
}