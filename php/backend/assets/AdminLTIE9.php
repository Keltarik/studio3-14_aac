<?php
/**
 * Created by PhpStorm.
 * User: zachot
 * Date: 02.08.15
 * Time: 0:25
 */

namespace backend\assets;


use yii\web\AssetBundle;

class AdminLTIE9 extends AssetBundle {
	public $sourcePath = null;
	public $jsOptions = [
		'position' => \yii\web\View::POS_HEAD,
		'condition' => 'lt IE 9'
	];
	public $js = [
		'//code.jquery.com/jquery-1.11.0.min.js',
	];
}