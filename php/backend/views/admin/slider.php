<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use backend\models\type\Slider;

/* @var $this yii\web\View */

$this->title = 'Слайдер';
$this->params['breadcrumbs'][] = $this->title;
$dataProvider = new ActiveDataProvider([
		'query' => Slider::find(),
		'sort'=> ['defaultOrder' => ['position'=>SORT_ASC]]
]);
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="tag-index">

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    'text:ntext',
                    'link:ntext',
                    [
                        'class' => 'yii\grid\DataColumn',
                        'attribute' => 'picture',
                        'value' => function ($model, $key, $index, $column)
                            {
                                return "<img src='".Yii::getAlias('@uploads/slider/'.$model->picture)."' width='300px'/>";
                            },
                        'format' => 'raw',
                    ],
                    [
                        'class' => 'yii\grid\DataColumn',
                        'attribute' => 'background',
                        'value' => function ($model, $key, $index, $column)
                            {
                                return "<img src='".Yii::getAlias('@uploads/slider/'.$model->background)."' width='300px'/>";
                            },
                        'format' => 'raw',
                    ],
                    [
                        'class' => 'yii\grid\DataColumn',
                        'value' => function ($model, $key, $index, $column)
                            {
                            	$sliders = \backend\models\type\Slider::find()->orderBy('position')->all();
                            	$moveUp = \yii\helpers\Url::to('@web/json/move-slider-up?id=' . $model->getAttribute('id') . '&pos=' . $model->getAttribute('position'));
                            	$disableUpClass = $model->getAttribute('position') == 0 ? "disabled" : "";
                            	$moveDown = \yii\helpers\Url::to('@web/json/move-slider-down?id=' . $model->getAttribute('id') . '&pos=' . $model->getAttribute('position'));
                            	$disableDownClass = $model->getAttribute('position') == (count($sliders)-1) ? "disabled" : "";
                                return "<a class='btn btn-sm btn-primary' href='".\yii\helpers\Url::to(['/edit/slider', 'id' => $model->id])."'>Редактировать</a>"." ".
                                "<a class='btn btn-sm btn-danger' href='".\yii\helpers\Url::to(['/delete/slider', 'id' => $model->id])."'>Удалить</a>"."<br/>".
                        		"<a class='btn btn-sm btn-default ".$disableUpClass."' href='".$moveUp."'>Вверх</a>"."<br/>".
                        		"<a class='btn btn-sm btn-warning ".$disableDownClass."' href='".$moveDown."'>Вниз</a>";
                            },
                        'format' => 'raw',
                    ],
                ],
            ]); ?>
        </div>
        <div>
            <a class="btn btn-primary"
               href="<?= \yii\helpers\Url::to('@web/edit/slider?id=0') ?>">Добавить</a>
        </div>
    </div>
</div>
