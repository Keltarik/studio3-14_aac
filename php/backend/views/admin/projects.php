<?php
/**
 * @var $projects \backend\models\type\Project[];
 */

use backend\models\type\Project;
?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Проекты</h1>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-body">
		<table class="table table-striped table-bordered table-hover data-table">
			<thead>
			<tr>
				<th>Картинка</th>
				<th>Заголовок</th>
				<th>Комнаты</th>
			</tr>
			</thead>
			<tfoot>
			<tr>
				<td colspan="2"><a class="btn btn-primary"
													 href="<?= \yii\helpers\Url::to('@web/edit/project?id=0') ?>">Добавить</a></td>
			</tr>
			</tfoot>
			<tbody>
			<?php $projects = Project::find()->orderBy(['id' => SORT_DESC])->all() ?>
			<?php foreach ($projects as $project) { ?>
				<tr>
					<td style="width: 200px;"><img style="width: 200px;" src="<?= $project->getThumbnailAttachmentUrl() ?>"></td>
					<td>
						<?= $project->getAttribute('title') ?>
						<div>
							<a href="<?= \yii\helpers\Url::to('@web/edit/project?id=' . $project->getAttribute('id')) ?>">Редактировать</a>
							<a class="text-danger js-delete"
								 href="<?= \yii\helpers\Url::to('@web/json/toggle-project-delete?id=' . $project->getAttribute('id')) ?>">Удалить</a>
						</div>
					</td>
					<td>
						<ul style="font-size: 10px;">
							<?php
							$rooms = $project->getRooms();
							foreach ($rooms as $room) {
								echo "<li>" . $room->getAttribute('title') . "</li>";
							}
							?>
						</ul>
					</td>
				</tr>
			<?php } ?>

			</tbody>
		</table>
	</div>
</div>