<?php
/**
 * Created by PhpStorm.
 * User: zachot
 * Date: 02.08.15
 * Time: 4:10
 */

namespace backend\models\helper;


use yii\base\Model;
use yii\helpers\Url;
use yii\validators\FileValidator;
use yii\web\UploadedFile;

class Upload extends Model {
	/**
	 * @var string
	 */
	private $_src;


	/**
	 * @param string $dir
	 */
	static function __dir($dir) {
		if (!is_dir($dir)) {
			mkdir($dir, 755);
			chmod($dir, 755);
		}
	}

	/**
	 * @param string $slug
	 * @return string
	 */
	static function __getRootBy($slug) {
		$_dir = array_reduce(
			[$slug, date("Y"), date('m'), date('j')],
			function ($last, $new) {
				$last .= '/' . $new;
				Upload::__dir(Url::to("@uploadsroot" . $last));
				return $last;
			}, ''
		);

		return $_dir;
	}


	public function getSrc() {
		return $this->_src;
	}

	/**
	 * @param $name
	 * @param FileValidator|null $validator
	 * @return $this
	 */
	public function saveImageToUrl($name, $validator = null) {
		$_temp = UploadedFile::getInstance($this, $name);
		$_dir = static::__getRootBy($name);
		$this->_src = $this->__saveImage($_temp, $name, $_dir, $validator);
		return $this;
	}

	/**
	 * @param UploadedFile $uploadedImage
	 * @param string $name
	 * @param string $dir
	 * @param FileValidator $validator
	 * @return null|string
	 */
	private function __saveImage($uploadedImage, $name, $dir, $validator) {

		if (is_null($validator))
			$validator = static::__getDefaultValidator();

		$_error = '';
		if ($validator->validate($uploadedImage, $_error)) {

			date_default_timezone_set('UTC');
			$_file = $dir . '/' . $this->__imageName($uploadedImage);
			$_url = Url::to("@uploadsroot/" . $_file);

			if ($uploadedImage->saveAs($_url)) {
				return "@uploads" . $_file;
			}
			$this->addError($name, "Cannot save file!");
		} else {
			$this->addError($name, $_error);
		}
		return null;
	}

	/**
	 * @param UploadedFile $uploadedImage
	 * @return null|string
	 */
	private function __imageName($uploadedImage) {
		return md5_file($uploadedImage->tempName)
		. "_"
		. $uploadedImage->size
		. '.'
		. $uploadedImage->extension;
	}

	/**
	 * @return FileValidator
	 */
	static function __getDefaultValidator() {
		return new FileValidator(
			[
				'maxSize' => 1024 * 1024 * 2,
				'extensions' => 'jpg,png,jpeg'
			]
		);
	}
}