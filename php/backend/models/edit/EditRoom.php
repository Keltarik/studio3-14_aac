<?php
/**
 * Created by PhpStorm.
 * User: zachot
 * Date: 05.08.15
 * Time: 5:03
 */

namespace backend\models\edit;


use backend\models\type\Room;
use yii\base\Model;

class EditRoom extends Model {
	public $title;
	public $text;

	public $menu_title;
	public $anchor;

	/**
	 * @var Room
	 */
	private $_room;

	public $id;


	public function rules() {
		return [
			[['title', 'text', 'menu_title', 'anchor'], 'required'],
			[['title', 'text', 'menu_title', 'anchor'], 'string']
		];
	}

	public function loadRoom($id) {
		$this->id = $id;
		/**
		 * @var $room Room
		 */
		$this->_room = $room = Room::findOne(["id" => $id]);
		if ($room) {
			$this->title = $room->getAttribute('title');
			$this->text = $room->getAttribute('text');
			$this->menu_title = $room->getAttribute('menu_title');
			$this->anchor = $room->getAttribute('anchor');
		}
	}

	public function save() {
		if (!$this->_room) {
			$this->addError('empty', 'true');
			$this->_room = new Room();
		}

		$this->_room->setAttribute('title', $this->title);
		$this->_room->setAttribute('text', $this->text);
		$this->_room->setAttribute('menu_title', $this->menu_title);
		$this->_room->setAttribute('anchor', $this->anchor);

		$this->_room->save();

		$this->id = $this->_room->getAttribute('id');

		$this->addErrors($this->_room->errors);

		return !$this->hasErrors();
	}

	/**
	 * @return mixed
	 */
	public function getId() {
		if ($this->_room)
			return $this->_room->getAttribute('id');
		return 0;
	}

	public function go() {
		return
			$this->load(\Yii::$app->request->post())
			&& $this->validate()
			&& $this->save();
	}
}