<?php
/**
 * Created by PhpStorm.
 * User: zachot
 * Date: 02.08.15
 * Time: 2:36
 */

namespace backend\models\edit;


use backend\models\type\Teammate;
use yii\base\Model;

class EditTeammate extends Model {

	public $name;
	public $photo;
	public $profession;
	public $about;

	public $main;
	public $past;
	/**
	 * @var Teammate
	 */
	private $_teammate;

	public function rules() {
		return [
			[['name', 'photo', 'profession'], 'required'],
			[['name', 'profession','about'], 'string'],
			[['photo', 'main', 'past'], 'number'],
		];
	}


	/**
	 * @param number $id
	 */
	public function loadTeammate($id) {
		/**
		 * @var $teammate Teammate
		 */
		$this->_teammate = $teammate = Teammate::findOne(["id" => $id]);
		if ($teammate) {
			$this->name = $teammate->getAttribute('name');
			$this->photo = $teammate->getAttribute('photo');
			$this->profession = $teammate->getAttribute('profession');
			$this->about = $teammate->getAttribute('about');
			$this->main = $teammate->getAttribute('main');
			$this->past = $teammate->getAttribute('past');
		}

	}

	/**
	 * @return bool
	 */
	public function save() {

		if (!$this->_teammate) {
			$this->addError('empty', 'true');
			$this->_teammate = new Teammate();
			$personUp = Teammate::find()->orderBy('position DESC')->one();
			$p = $personUp->getAttribute('position');
			$this->_teammate->setAttribute('position', $p+1);
		}

			$this->_teammate->setAttribute('name', $this->name);
			$this->_teammate->setAttribute('photo', $this->photo);
			$this->_teammate->setAttribute('profession', $this->profession);
			$this->_teammate->setAttribute('about', $this->about);
			$this->_teammate->setAttribute('main', $this->main);
			$this->_teammate->setAttribute('past', $this->past);

			$this->_teammate->save();

			$this->addErrors($this->_teammate->errors);

		return !$this->hasErrors();
	}

	/**
	 * @return mixed
	 */
	public function getId() {
		if ($this->_teammate)
			return $this->_teammate->getAttribute('id');
		return 0;
	}

	/**
	 * @return bool
	 */
	public function go() {
		return
			$this->load(\Yii::$app->request->post())
			&& $this->validate()
			&& $this->save();
	}
}