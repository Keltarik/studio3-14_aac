<?php
/**
 * @property string $name
 * @property string $slug
 * @property integer $id
 */
namespace common\models\type;


use yii\db\ActiveRecord;

class Tag extends ActiveRecord {

    public static function tableName()
    {
        return '{{%tag}}';
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Тег',
            'slug' => 'ЧПУ-ссылка',
        ];
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'slug'], 'unique'],
            [['name', 'slug'], 'string', 'max' => 255],
        ];
    }
}