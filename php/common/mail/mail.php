<?php
use yii\helpers\Html;
use yii\helpers\Url;


/**
 * @var $this \yii\web\View view component instance
 * @var string $user_name
 * @var string $user_phone
 * @var string $user_email
 * @var string $user_text
 */

?>
<h1
	style="line-height: 32px;  font-size: 32px; border-left: 10px solid #10BDA9; margin-top: 10px; padding: 0 25px 0 15px">
	Новое сообещиние</h1>

<div style="padding: 0 25px;"><h4>Имя: <?= $user_name ?></h4></div>
<div style="padding: 0 25px;"><h5>Контакты:<?= $user_phone ?> <?= $user_email ?></h5></div>
<div style="padding: 0 25px;">Сообщение:</div>
<p style="padding: 0 25px;">
	<?= $user_text ?>
</p>
