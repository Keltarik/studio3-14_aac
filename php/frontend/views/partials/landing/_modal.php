<?php

/* @var $callbackModel \frontend\models\form\AddReport */

use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="modal-wrapper">
    <div class="modal">
        <a class="close-modal" href="javascript:;">
            <img src="<?= Yii::getAlias('@web') ?>/img/icons/close.png" alt="">
        </a>
        <h2 class="title-line">
            Оставьте свои <br> контакты
            
        </h2>
        <?= Html::beginForm(Url::to('@web/consult'), 'POST', ['id' => 'modal-callback-form']) ?>
            <p class="order__form-info">
        	Мы отправим Вам всю информацию и перезвоним в течение часа
            </p>
            <label for="order_name3">
                <i class="icon"><svg><use xlink:href="#icon_person"></use></svg></i>
                <?= Html::activeTextInput($callbackModel, 'user_name', ['id' => 'order_name3', 'placeholder' => 'Ваше имя']) ?>
            </label>
            <label for="order_phone3">
                <i class="icon"><svg><use xlink:href="#icon_phone"></use></svg></i>
                <?= Html::activeTextInput($callbackModel, 'user_phone', ['id' => 'order_phone3', 'placeholder' => 'Телефон']) ?>
            </label>
            <label for="order_email3">
                <i class="icon"><svg><use xlink:href="#icon_email"></use></svg></i>
                <?= Html::activeTextInput($callbackModel, 'user_email', ['id' => 'order_email3', 'placeholder' => 'Электронная почта']) ?>
            </label>

            <?= Html::submitButton('<span>Оставить заявку</span><i class="icon"><svg><use xlink:href="#icon_pen"></use></svg></i>', [
                'class' => 'order__form-submit btn',
                'id' => 'order_submit'
            ]) ?>

            <p class="order__form-person">
                Нажимая на кнопку «Оставить заявку»,<br>
                <span>Вы соглашаетесь на <a href="<?= Yii::getAlias('@web') ?>/pdf/policy.pdf" data-fancybox="">обработку персональных данных.</a></span>
            </p>
        <?= Html::endForm() ?>
    </div>
</div>

<?php

$script = <<< JS



    $('body').delegate('#modal-callback-form', 'submit', function () {
        
        $(this).validate({
            rules:{
                'AddReport[user_name]':{
                    required: true,
                    minlength: 3,
                    maxlength: 20
                },
                'AddReport[user_phone]':{
                    required: true,
                    minlength: 7,
                    maxlength: 20
                },
                'AddReport[user_email]':{
                    required: true,
                    email: true
                }
           },
          
           messages:{
                'AddReport[user_name]':{
                    required: "Это поле обязательно для заполнения",
                    minlength: "Имя должно содержать минимум 3 символа",
                    maxlength: "Максимальное число символов - 20"
                },
                'AddReport[user_phone]':{
                    required: "Это поле обязательно для заполнения",
                    minlength: "Телефон должен содержать минимум 7 символов",
                    maxlength: "Телефон может содержать максимум 20 символов",
                },
                'AddReport[user_email]':{
                    required: "Это поле обязательно для заполнения",
                    email: "Проверьте правильность введенного email"
                }
            },
        });
        
        
        if ($(this).valid()) {
            $.ajax({
                method: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                success: function(data) {
                    if (data.error) {
                        
                        var errorText = '';
                        
                        $.each( data.message, function( key, value ) {
                          errorText += value + '; ';
                        });
                        
                        alert(errorText);
                    }else{
                        alert(data.message);
                        $('#modal-callback-form')[0].reset();
                    }
                },
                error: function() {
                    alert('Возникла ошибка при отправке запроса.');
                }
            });
        }        
        
        return false;
    });
    
    
    
JS;

$this->registerJs($script, yii\web\View::POS_END);

?>