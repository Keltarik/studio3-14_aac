<div class="press wrapper">
    <h2 class="press__title title-line">
        Пресса о нас
    </h2>
    <div class="press__blocks">
        <div class="press__block">
            <div class="press__block-header">
                <img src="<?= Yii::getAlias('@web') ?>/img/press/1.png" alt="" class="press__block-header-logo">
                <a href="javascript:;" class="press__block-header-title">
                    Организация интерьера<br>
                    «малогабариток»
                </a>
            </div>
            <div class="press__block-content">
                <p class="press__block-content-description">
                    Стили дизайна в небольших квартирах могут быть<br>
                    разными, главное — их функциональность. Алена<br>
                    Горшкова, дизайнер-архитектор «Студии 3.14»,<br>
                    советует уделить основное внимание<br>
                    использованию свободного пространства и<br>
                    зонированию помещений.
                </p>
            </div>
        </div>
        <div class="press__block">
            <div class="press__block-header">
                <img src="<?= Yii::getAlias('@web') ?>/img/press/2.png" alt="" class="press__block-header-logo">
                <a href="javascript:;" class="press__block-header-title">
                    Проект недели<br>
                    на InMyRoom
                </a>
            </div>
            <div class="press__block-content">
                <p class="press__block-content-description">
                    В этой сталинке будет жить уже третье поколение семьи – и, конечно, у молодых хозяев была мечта сделать интерьер более современным и удобным.<br>
                    Дизайнеры из «Студии 3.14» воплотили ее в реальность.
                </p>
            </div>
        </div>
        <div class="press__block">
            <div class="press__block-header">
                <img src="<?= Yii::getAlias('@web') ?>/img/press/3.png" alt="" class="press__block-header-logo">
                <a href="javascript:;" class="press__block-header-title">
                    Скандинавия на Ленинском<br>
                    проспекте
                </a>
            </div>
            <div class="press__block-content">
                <p class="press__block-content-description">
                    Светлая и изящная квартира в скандинавском стиле стала новым проектом дизайнеров из команды «Студия 3.14». Элегантное решение для молодой семьи с ребенком было найдено в светлой цветовой гамме, легком минимализме и нескольких цветовых акцентах.
                </p>
            </div>
        </div>
        <div class="press__block">
            <div class="press__block-header">
                <img src="<?= Yii::getAlias('@web') ?>/img/press/4.png" alt="" class="press__block-header-logo">
                <a href="javascript:;" class="press__block-header-title">
                    Проект недели<br>
                    на InMyRoom
                </a>
            </div>
            <div class="press__block-content">
                <p class="press__block-content-description">
                    Кессонный потолок в гостиной, деревянные обои в ТВ-зоне, узоры из декоративных молдингов на стенах – хозяева этой просторной квартиры открыты для интересных дизайнерских решений
                </p>
            </div>
        </div>
        <div class="press__block">
            <div class="press__block-header">
                <img src="<?= Yii::getAlias('@web') ?>/img/press/1.png" alt="" class="press__block-header-logo">
                <a href="javascript:;" class="press__block-header-title">
                    Перепланировка<br>
                    для жизни
                </a>
            </div>
            <div class="press__block-content">
                <p class="press__block-content-description">
                    В жилом фонде осталось немало хрущевок и брежневок, планировки которых зачастую оставляют желать лучшего. Специалисты «Студии 3.14» предлагают варианты их использования.
                </p>
            </div>
        </div>
        <div class="press__block">
            <div class="press__block-header">
                <img src="<?= Yii::getAlias('@web') ?>/img/press/2.png" alt="" class="press__block-header-logo">
                <a href="javascript:;" class="press__block-header-title">
                    Экономим пространство
                </a>
            </div>
            <div class="press__block-content">
                <p class="press__block-content-description">
                    Сейчас в дизайне очень востребован так называемый скандинавский стиль. Это
                    недорогая практичная мебель, минимум украшений, максимум функциональности, — рассказывает руководитель «Студии 3.14»
                    Тимур Абдрахманов.
                </p>
            </div>
        </div>
    </div>
</div>