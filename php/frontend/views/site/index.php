<?php

/* @var $this \yii\web\View */
/* @var $callbackModel \frontend\models\form\AddReport */

$this->title = '«Студия 3.14» — дизайн интерьера под ключ';
$this->registerMetaTag(['name' => 'description', 'content' => '«Студия 3.14» — стильный и практичный дизайн интерьера в Москве и Подмосковье.']);

?>

<?= $this->render('../partials/landing/_navigation') ?>

<?= $this->render('../partials/landing/_header') ?>

<?= $this->render('../partials/landing/_steps-form') ?>

<?= $this->render('../partials/landing/_our-projects') ?>

<?= $this->render('../partials/landing/_ready-styles') ?>

<?= $this->render('../partials/landing/_rewards') ?>

<?= $this->render('../partials/landing/_price', [
    'callbackModel' => $callbackModel
]) ?>

<?= $this->render('../partials/landing/_press-about-us') ?>

<?= $this->render('../partials/landing/_team') ?>

<?= $this->render('../partials/landing/_draft') ?>

<?= $this->render('../partials/landing/_money') ?>

<?= $this->render('../partials/landing/_repairs') ?>

<?= $this->render('../partials/landing/_order', [
    'callbackModel' => $callbackModel
]) ?>

<?= $this->render('../partials/landing/_contacts') ?>

<?= $this->render('../partials/landing/_footer') ?>

<?= $this->render('../partials/landing/_modal', [
    'callbackModel' => $callbackModel
]) ?>