<?php
/**
 * @var $project \common\models\type\Project
 */
use yii\helpers\Url;


$rooms = $project->getRooms();
$reviews = $project->getReviews();
$partners = $project->getPartners();

$this->title = $project->getAttribute('title-page');
$this->registerJsFile(Url::to("@web/assets/jquery.min.js"), ['position' => $this::POS_END]);
?>

<div class="wrapper">
	<div style="background-image:url(<?= $project->getBackBlurAttachmentUrl() ?>)" class="wrapper-img"></div>

	<div class="wrapper-back">

		<div class="page_head">
			<div class="container">
				<div class="page_head-content">
					<div style="background-image:url(<?= $project->getThumbnailAttachmentUrl() ?>)"
							 class="page_head-content-img"></div>
					<div class="page_head-content-title col-xs-offset-1"><h1></h1></div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-xs-offset-11 col-xs-1 site-button">

					<div class="site-page-menu">
						<ul>
							<?php foreach ($rooms as $room) { ?>
								<li>
									<a href="#<?= $room->getAttribute('anchor') ?>"
										 class="js-scroll-link"><?= $room->getAttribute('menu_title') ?></a>
								</li>
							<?php } ?>
						</ul>
					</div>

					<div class="site-button-over">
						<a href="#consulting"
							 class="js-scroll-link site-button-link site-button-link__page">Заказать<br>такой же<br>дизайн</a></div>
				</div>
			</div>
		</div>
		<div class="container page-content">
			<div class="row">
				<div class="col-xs-offset-1 col-xs-10">
					<div class="page_content">
						<h1 class="text-uppercase"><?= $project->getAttribute('title') ?></h1>
                    </div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-10 col-xs-offset-1">
					<div class="page_content">
						<div class="pull-right" style="margin: 0 0 30px 30px;">
							<div class="page__team">
								<div class="page__team-header">Команда проекта</div>
								<?php
								$team = $project->getTeam();
								foreach ($team as $inde => $teammate) {
									/**
									 * @var $t \common\models\type\Teammate
									 */
									$t = \common\models\type\Teammate::findOne($teammate['id']);
									?>
									<div class="page__team-mate">

										<a class="js-person no-border"
											 href="<?= \yii\helpers\Url::to('@web/person/' . $t->getAttribute('id')) ?>">


											<div class="team-mate-photo">
												<div style="background-image:url(<?= $t->getPhotoUrl() ?>)" class="team-mate-photo-img"></div>
											</div>
											<div class="team-mate-text">
												<div class="team-mate-text-prof"><?= $teammate['role'] ?></div>
												<div class="team-mate-text-name"><?= $t->getAttribute('name') ?></div>
											</div>

										</a>

									</div>
								<?php } ?>


								<div class="page__team-footer">
									<div class="page__team-footer-map"><?= $project->getAttribute('address') ?></div>
								</div>
							</div>
						</div>
						<?= $project->getAttribute('text') ?>


						<?php foreach ($rooms as $room) { ?>
							<h2 id="<?= $room->getAttribute('anchor') ?>"><?= $room->getAttribute('title') ?></h2>
							<?= $room->getAttribute('text') ?>
						<?php } ?>

                        <?php if(count($partners) > 0) { ?>
                            <h3 style="margin-bottom: 5px;">Партнеры</h3>
                        <?php } ?>

                        <ul class="projects">
                        <?php foreach ($partners as $partner) { ?>
                            <li>
                                <a class="js-person" href="<?= \yii\helpers\Url::to('@web/partner/' . $partner->getAttribute('id')) ?>"><?= $partner->getAttribute('name') ?></a>
                            </li>
                        <?php } ?>
                        </ul>
                        <br/>

						<?php if ($reviews && !empty($reviews)) {
							echo "<h1>Отзывы клиентов</h1>";
							foreach ($reviews as $review) { ?>
								<div style="margin-bottom: 50px;">
									<div class="page__team pull-right" style="margin: 0 0 20px 30px;">
										<!--<div class="page__team-header">Заказчик</div>-->
										<div class="page__team-mate">
											<div class="team-mate-photo">
												<div style="background-image:url(<?= $review->getPhotoUrl() ?>)"
														 class="team-mate-photo-img"></div>
											</div>
											<div class="team-mate-text">
												<div class="team-mate-text-prof"><?= $review->getAttribute('author_name') ?></div>
												<div class="team-mate-text-name"><?= $review->getAttribute('author_profession') ?></div>
												<br>
											</div>
										</div>
									</div>
									<?= $review->getAttribute('text') ?>
								</div>
							<?php }
                            echo "<br/>";
						} ?>

                        
					</div>
				</div>
			</div>


			
			<div class="row">
				<div class="col-xs-10 col-xs-offset-1">
					<div class="text-center">Можете сохранить идеи этого дизайн-проекта в своей любимой социальной сети!
					</div>
					<div class="social page-social text-center">
					<a href="https://www.facebook.com/place4life" data-social="facebook" class="social-share social-icon social-icon-fb"></a>
					<a href="https://vk.com/place4life" data-social="vkontakte" class="social-share social-icon social-icon-vk"></a>
					<a href="https://twitter.com/place4life" data-social="twitter" class="social-share social-icon social-icon-tw"></a>
					<a target="_blank" href="http://www.houzz.ru/pro/studio314/" class="social-icon social-icon-houzz"></a>
					<!--a(data-social="in").social-share.social-icon.social-icon-in(href='http://instagram.com/place4life')-->
					<!--a(data-social="pin").social-share.social-icon.social-icon-pin(href='http://www.pinterest.com/studiotri14/')-->
					</div>
				</div>
			</div>			
			<div class="row" id="consulting">
				<div class="col-xs-offset-1 col-xs-10">
					<div class="form">
						<form action="<?= \yii\helpers\Url::to("@web/consult") ?>" method="post">
							<input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
							<h4 class="form-title text-bold text-center">Заявка на
								дизайн-проект</h4>

							<input type="hidden" name="AddReport[project_id]" value="<?= $project->getAttribute('id') ?>">

							<div class="form-message text-center">Понравился проект и&nbsp;хотите заказать в&nbsp;аналогичном стиле?<br>
								Оставьте заявку прямо сейчас и&nbsp;получите скидку на&nbsp;дизайн-проект 3%!
							</div>
							<div class="form-input">
								<div class="row">
									<div class="col-sm-6 col-xs-12">
										<div class="form-group"><input type="text" name="AddReport[user_name]" placeholder="Имя"
																									 class="form-control"></div>
									</div>
									<div class="col-sm-6 col-xs-12">
										<div class="form-group"><input type="text" name="AddReport[user_phone]" placeholder="Телефон"
																									 class="form-control"></div>
									</div>

								</div>
                                <div class="row form-email">
                                    <div class="col-xs-12">
                                        <div class="form-group"><input tabindex="-1" type="email"
                                                                       name="AddReport[user_email]"
                                                                       placeholder="E-mail"
                                                                       class="form-control"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                                <textarea rows="4" name="AddReport[user_text]" placeholder="Комментарий (необязательно)"
                                                          class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
							</div>
							<div class="text-center"><input type="submit" value="Заказать дизайн-проект"
																							class="btn btn-danger btn-lg form-submit">
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-10 col-xs-offset-1">
					<div class="page_content">
						<h1>Комментарии</h1>
						<div id="hypercomments_widget"></div>
						<script type="text/javascript">
							_hcwp = window._hcwp || [];
							_hcwp.push({widget:"Stream", widget_id: 72895});
							(function() {
							if("HC_LOAD_INIT" in window)return;
							HC_LOAD_INIT = true;
							var lang = (navigator.language || navigator.systemLanguage || navigator.userLanguage || "en").substr(0, 2).toLowerCase();
							var hcc = document.createElement("script"); hcc.type = "text/javascript"; hcc.async = true;
							hcc.src = ("https:" == document.location.protocol ? "https" : "http")+"://w.hypercomments.com/widget/hc/72895/"+lang+"/widget.js";
							var s = document.getElementsByTagName("script")[0];
							s.parentNode.insertBefore(hcc, s.nextSibling);
							})();
						</script>
						<a href="http://hypercomments.com" class="hc-link" title="comments widget">comments powered by HyperComments</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=NmLod2YJHKr099u/DjDsGPQlZ8edsRHX3yYdAZpg3JX/g9DjrErl6agdmEG8/mAf0WK9yVSiArmBRYJYkX*eEbHEyMfVeYejlSKQojf0RkT4siNvBtBG6rBzWwGWLmxF5tcj3qbiaPZMka312DkXuxPGTSLcJAkBGu4FI7AhHes-';</script>
