<?php
/**
 * @var $partner \frontend\models\type\Partner
 */
?>

<div class="person_popup" style="top: -4.5px;">
	<button title="Close (Esc)" type="button" class="mfp-close"></button>
	<div class=" person_popup-fill">
		<div class="row ">
			<div class="col-sm-3 col-xs-12">
				<div style="background-image: url(<?= $partner->getLogoUrl() ?>);"
						 class="person_popup-photo employee-photo photo partner-photo"></div>
			</div>
			<div class="col-sm-9 col-xs-12">
				<div class="person_popup-wrapper">
					<h3 class="person_popup-name employee-name"><?= $partner->getAttribute('name') ?></h3>

					<div class="person_popup-citation"><?= $partner->getAttribute('about') ?></div>
                    <div class="person_popup-projects">
                        <?php
                        $projects = ($partner->getProjects());

                        if ($projects) {
                            ?>
                            <h3>Проекты:</h3>

                            <ul>
                                <?php foreach ($projects as $project) { ?>
                                    <li><a href="<?= $project->getUrl() ?>"><?= $project->getAttribute('title') ?></a></li>
                                    <?php
                                } ?>
                            </ul>
                            <?php
                        }
                        ?>

                    </div>
				</div>
			</div>
		</div>
	</div>
</div>