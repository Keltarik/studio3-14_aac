<?php
/**
 */
use yii\helpers\Url;

$this->title = 'Спасибо за заявку';
?>


<div class="wrapper">
	<div style="background-image:url(<?= Url::to('@web/img/old_template_img/thankyou/thankyou_blur.jpg') ?>)" class="wrapper-img"></div>

	<div class="wrapper-back thank_you-wrapper-back">

		<div class="page_head">
			<div class="container">
				<div class="page_head-content">
					<div style="background-image:url(<?= Url::to('@web/img/old_template_img/thankyou/thankyou.jpg') ?>)" class="page_head-content-img"></div>
					
					<div class="thank_you-text">
						<p class="page__text text-center">Если вы&nbsp;планируете заказать дизайн квартиры или дома, вы&nbsp;можете скачать соответствующую анкету для подготовки дизайн-проекта и&nbsp;начать ее&nbsp;заполнение.</p> 
						<p class="page__text text-center">Это позволит вам более точно сформулировать ваши пожелания, а&nbsp;нам&nbsp;— получить представление о&nbsp;задачах.</p>
                        <p class="page__text text-center">
                            <a class="red-link text-left" href="<?= Url::to("@web/img/old_template_img/thankyou/Anketa_TZ_kvartira.docx") ?>">Скачать анкету на дизайн-проект квартиры</a>
                        </p>
                        <p class="page__text text-center">
                            <a class="red-link text-right" href="<?= Url::to("@web/img/old_template_img/thankyou/Anketa_TZ_dom.docx") ?>">Скачать анкету на дизайн-проект дома/таунхауса</a>
                        </p>
					</div>
				</div>
			</div>
		</div>
		<div class="container page-content">
			<div class="row">
				<div class="col-xs-10 col-xs-offset-1">
					<div class="page_content">
						<h3 class="text-center no-margin-bottom">Следите за нами в социальных сетях</h3>

						<div class="social text-center margin-top-10">
							<a target="_blank" href="https://www.facebook.com/place4life" class="social-icon social-icon-fb"></a>
							<a target="_blank" href="https://vk.com/place4life" class="social-icon social-icon-vk"></a>
							<a target="_blank" href="https://twitter.com/place4life" class="social-icon social-icon-tw"></a>
							<a target="_blank" href="http://instagram.com/place4life" class="social-icon social-icon-in"></a>
							<a target="_blank" href="http://www.pinterest.com/place4life" class="social-icon social-icon-pin"></a>
                            <a target="_blank" href="http://www.houzz.ru/pro/studio314/" class="social-icon social-icon-houzz"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
