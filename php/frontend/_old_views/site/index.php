<?php
/**
 * @var $this \yii\web\View
 */

use yii\helpers\Url;

$this->registerJsFile("//api-maps.yandex.ru/2.1/?lang=ru_RU", ['position' => $this::POS_END]);
$this->registerJsFile(Url::to("@web/js/YaMap.js"), ['position' => $this::POS_END]);

$this->title = '&laquo;Студия&nbsp;3.14&raquo;&nbsp;&mdash; дизайн интерьера &laquo;под ключ&raquo;';
$slider = \common\models\type\Slider::find()->all();
$background_array = \yii\helpers\ArrayHelper::getColumn(\common\models\type\Slider::find()->asArray()->all(), 'background');
foreach ($background_array as &$value) {
    $value = Url::to("@uploads/slider/".$value);
}
?>


<?php //echo $this->renderFile('@app/views/parts/1_top-slider.php', []); ?>


<div class="wrapper">
	<div class="wrapper-back">
		<div class="container">
			<div class="row">
				<div class="col-xs-offset-1 col-xs-10">
					<div class="signature">
					</div>
				</div>
				<div class="col-xs-1 site-button">
					<div class="site-button-over">
						<a href="#consulting" class="js-scroll-link site-button-link">Заказать &nbsp;дизайн</a>
					</div>
				</div>
			</div>
		</div>

    <?php echo $this->renderFile('@app/views/parts/1_header_text.php', []); ?>
		<?php echo $this->renderFile('@app/views/parts/4_portfolio.php', []); ?>
		<?php echo $this->renderFile('@app/views/parts/3_price.php', []); ?>
      <?php echo $this->renderFile('@app/views/parts/8_form.php', []); ?>
      <?php echo $this->renderFile('@app/views/parts/7_team.php', []); ?>
      <?php echo $this->renderFile('@app/views/parts/5_reviews.php', []); ?>
      <?php echo $this->renderFile('@app/views/parts/2_service_new.php', []); ?>
        <?php echo $this->renderFile('@app/views/parts/9_how-work.php', []); ?>



        <?php //echo $this->renderFile('@app/views/parts/6_opportunities.php', []); ?>
        <?php echo $this->renderFile('@app/views/parts/77_press.php', []); ?>
        <?php echo $this->renderFile('@app/views/parts/8_form.php', []); ?>

	</div>
</div>
