<?php
$this->registerJsFile('/js/portfolio-full.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<div id="portfolio" class="portfolio">
  <div class="container">
    <div class="row">
      <div class="col-xs-10 col-xs-offset-1" id="items-container">
        <h1 class="first-on-page">Наши проекты</h1>
        <div>
          <ul class="nav portfolio-tags">
          <?php foreach (\common\models\type\Tag::find()->all() as $tag):?>
            <li><a class="tag-filter-link" href="#portfolio<?=$tag->slug?>" data-id-slug="<?=$tag->slug?>"><?=$tag->name?></a></li>
          <?php endforeach?>
            <li><a href="#portfolio" id="clear-tags" style="display: none">x</a></li>
          </ul>
        </div>
        <div class="portfolio-gallery">
          <div class="row view">
          <?php
            $string = file_get_contents(__DIR__ . "/../../../../settings/portfolio.json");
            $json = json_decode($string, true);
            $size_counter = 0;
            $row_counter = 0;
            foreach ($json as $pr) {
              if (!isset($pr['size'])) continue;
              $pr['image_url'] = (isset($pr['image_url'])) ? $pr['image_url'] : "";
              $pr['slug'] = (isset($pr['slug'])) ? $pr['slug'] : "";
              $pr['title'] = (isset($pr['title'])) ? $pr['title'] : "";
              $pr['description'] = (isset($pr['description'])) ? $pr['description'] : "";
              $pr['tag'] = (isset($pr['tag'])) ? $pr['tag'] : "";
              $pr['tag_slug'] = (isset($pr['tag_slug'])) ? $pr['tag_slug'] : "";
              if (($size_counter > 0) && ($size_counter >= 12)) {
                $row_counter++;
                $size_counter = 0;
                if ($row_counter > 4) {
                  echo '</div><div class="row">';
                } else {
                  echo '</div><div class="row view">';
                }
              }
          ?>
              <div class="col-xs-12 col-sm-<?= $pr['size'] ?>">
                <div style="background-image: url(<?= $pr["image_url"] ?>)" class="portfolio-gallery-thumbnail" data-tag="<?= $pr['tag'] ?>" data-tag-slug="<?= $pr['tag_slug'] ?>">
                  <a href="<?= \yii\helpers\Url::to("@web/project/" . $pr['slug']) ?>">
                    <span><?= $pr["title"] ?></span><span class="area"><?= $pr["description"] ?></span>
                  </a>
                </div>
              </div>
          <?php
              $size_counter += intval($pr['size']);
            }
          ?>
          </div>
        </div>
        <div class="col-xs-10 col-xs-offset-1 text-center">
          <a class="red-link portfolio-gallery-more">Другие проекты</a>
        </div>
      </div>
    </div>
  </div>
</div>
