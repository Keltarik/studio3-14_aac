<div id="reviews" class="reviews">
	<div class="container">
		<div class="row">
			<div class="col-xs-10 col-xs-offset-1"><h1>Отзывы</h1>

				<div id="review_carousel" data-ride="carousel" data-interval="false"
						 class="carousel slide">
					<div role="listbox" class="carousel-inner">
						<?php
						/**
						 * @var $reviews \common\models\type\Review[]
						 */
						$reviews = \common\models\type\Review::find()->orderBy('id desc')->all();


						foreach ($reviews as $index => $review) {
							$project = $review->getProject();

							$url = ($project)
								? (\yii\helpers\Url::to('@web/project/' . $project->getAttribute('slug')))
								: "";
							?>
							<div class="item <?= ($index == 5) ? 'active' : '' ?>">
								<div class="review">
									<div class="review-author">
										<div class="review-author-photo">
											<img src="<?= $review->getPhotoUrl() ?>">
										</div>
										<div class="review-author-name"><?= $review->getAttribute('author_name') ?></div>
										<div class="review-author-prof"><?= $review->getAttribute('author_profession') ?></div>
									</div>
									<div class="review-text">
										<div class="review-text-title">
											<?php if ($url) { ?>
												<a href="<?= $url ?>"
													 class="red-link"><?= $review->getAttribute('main_title') ?></a>
											<?php } else { ?>
												<span><?= $review->getAttribute('main_title') ?></span>
											<?php } ?>
										</div>
										<div class="review-text-content"><?= strip_tags($review->getAttribute('main_text')) ?></div>
									</div>
									<?php if ($review->getAttribute('video')) { ?>

										<div class="review-video"><a
												style="background-image:url(http://img.youtube.com/vi/<?= $review->getVideoId() ?>/maxresdefault.jpg);"
												target="_blank"
												href="<?= $review->getAttribute('video') ?>"
												class="review-video-link"><span class="icon"></span></a>
										</div>
										<?php
									} ?>

								</div>
							</div>
							<?php
						}
						?>
					</div>
					<a href="#review_carousel" data-slide="prev"
						 class="left carousel-control"><span
							class="icon-carousel icon-carousel-prev"></span></a><a
						href="#review_carousel" data-slide="next"
						class="right carousel-control"><span
							class="icon-carousel icon-carousel-next"></span></a></div>
			</div>
		</div>
	</div>
</div>