<div id="consulting" class="container">
	<div class="row">
		<div class="col-xs-offset-1 col-xs-10">
			<div class="form">
				<form action="<?= \yii\helpers\Url::to("@web/consult") ?>" method="post" onsubmit="yaCounter24474014.reachGoal('order'); ga('send', 'event', 'Goal', 'order'); roistat.event.send('order');">

					<input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
					<h4
						class="form-title text-bold text-center">
                        Заявка на&nbsp;бесплатную консультацию</h4>

					<div class="form-message text-center">
                        Оставьте заявку на бесплатную консультацию прямо сейчас <br/>и мы свяжемся с вами в течение дня
                        <br>
					</div>
					<div class="form-input">
						<div class="row">
							<div class="col-sm-6 col-xs-12">
								<div class="form-group"><input type="text" name="AddReport[user_name]"
																							 placeholder="Имя"
																							 class="form-control"></div>
							</div>
							<div class="col-sm-6 col-xs-12">
								<div class="form-group"><input type="text" name="AddReport[user_phone]"
																							 placeholder="Телефон"
																							 class="form-control"></div>
							</div>

						</div>
                        <div class="row form-email">
                            <div class="col-xs-12">
                                <div class="form-group"><input tabindex="-1" type="email"
                                                               name="AddReport[user_email]"
                                                               placeholder="E-mail"
                                                               class="form-control"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                                <textarea rows="4" name="AddReport[user_text]" placeholder="Комментарий (необязательно)"
                                                          class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
					</div>
					<div class="text-center">
						<input type="submit" value="Отправить заявку" class="btn btn-danger btn-lg form-submit" onclick="yaCounter24474014.reachGoal('order-button'); ga('send', 'event', 'Goal', 'order-button'); roistat.event.send('order-button');">
					</div>
				</form>
			</div>
		</div>
	</div>
</div>