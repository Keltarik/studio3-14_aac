<div id="services">
	<div class="container">
		<div class="row">
			<div class="col-xs-10 col-xs-offset-1">
				<h1>Услуги</h1>
				<div class="row">
					<div class="col-xs-12 col-sm-4">
					    <div class="service-icon">
							<svg width="157" height="140" id="pen-marks" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 157 157">
							  <defs>
							    <style>
							      .cls-1, .cls-2 {
							        fill: #fff;
							      }
							
							      .cls-2, .cls-3 {
							        stroke: #000;
							        stroke-linecap: round;
							        stroke-linejoin: round;
							        stroke-width: 3px;
							      }
							
							      .cls-3 {
							        fill: none;
							      }
							    </style>
							  </defs>
							  <rect class="cls-1" width="157" height="157"/>
							  <g>
							    <rect id="_Rectangle_" data-name="&lt;Rectangle&gt;" class="cls-2" x="47.08" y="39.36" width="62.46" height="81.68"/>
							    <rect id="_Rectangle_2" data-name="&lt;Rectangle&gt;" class="cls-2" x="63.71" y="35.73" width="30.31" height="9.55"/>
							    <rect id="_Rectangle_3" data-name="&lt;Rectangle&gt;" class="cls-3" x="54.84" y="58.98" width="12.44" height="12.44"/>
							    <rect id="_Rectangle_4" data-name="&lt;Rectangle&gt;" class="cls-3" x="54.84" y="86.43" width="12.44" height="12.44"/>
							  </g>
							  <g id="pen" data-name="&lt;Group&gt;">
							    <polygon id="_Path_" data-name="&lt;Path&gt;" class="cls-1" points="105.55 43.3 103.82 60.36 141.55 30.94 134.05 21.09 105.55 43.3"/>
							    <polygon id="_Path_2" data-name="&lt;Path&gt;" class="cls-2" points="101.45 62.2 139.5 32.54 153.95 21.28 146.33 11.52 93.84 52.44 84.08 67.89 101.45 62.2"/>
							    <line id="_Path_3" data-name="&lt;Path&gt;" class="cls-3" x1="138.51" y1="18.27" x2="145.49" y2="27.22"/>
							    <line id="_Path_4" data-name="&lt;Path&gt;" class="cls-3" x1="94.57" y1="52.53" x2="101.54" y2="61.48"/>
							  </g>
							  <g id="mark1" data-name="&lt;Group&gt;">
							    <path d="M57.38,64.19a27.06,27.06,0,0,1,1.82,3.54,1.52,1.52,0,0,0,2.51.66c4.14-4.39,7.33-9.6,11.47-14,1.33-1.4-.79-3.53-2.12-2.12-4.14,4.39-7.33,9.6-11.47,14l2.51,0.66A32.26,32.26,0,0,0,60,62.68c-0.9-1.71-3.49-.19-2.59,1.51h0Z"/>
							  </g>
							  <g id="mark2" data-name="&lt;Group&gt;">
							    <path d="M57.38,91.19a27.06,27.06,0,0,1,1.82,3.54,1.52,1.52,0,0,0,2.51.66c4.14-4.39,7.33-9.6,11.47-14,1.33-1.4-.79-3.53-2.12-2.12-4.14,4.39-7.33,9.6-11.47,14l2.51,0.66A32.26,32.26,0,0,0,60,89.68c-0.9-1.71-3.49-.19-2.59,1.51h0Z"/>
							  </g>
							</svg>
							<a class="red-link">Дизайн интерьеров</a>
							<div class="service-text">
							Мы&nbsp;разрабатываем дизайн-проекты жилых интерьеров: квартир, апартаметов, загородных домов и&nbsp;таунхаусов. Заказчики «Студии&nbsp;3.14» могут выбрать подходящий им&nbsp;пакет услуг по&nbsp;созданию интерьера, а&nbsp;наша задача&nbsp;— создать уникальный интерьер, отражающий пожелания и&nbsp;потребности заказчиков.
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4">
						<div class="service-icon">
							<svg id="sofa-lamp" width="190" height="140" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 190 140">
							  <defs>
							    <style>
							      .cls-12, .cls-32 {
							        fill: #fff;
							      }
							
							      .cls-22, .cls-42 {
							        fill: none;
							      }
							
							      .cls-22, .cls-32, .cls-42 {
							        stroke: #000;
							        stroke-width: 3px;
							      }
							
							      .cls-22, .cls-32 {
							        stroke-linecap: round;
							        stroke-linejoin: round;
							      }
							
							      .cls-42 {
							        stroke-miterlimit: 10;
							      }
							    </style>
							  </defs>
							  <rect class="cls-12" width="190" height="140"/>
							  <g>
							    <rect id="_Rectangle_" data-name="&lt;Rectangle&gt;" class="cls-22" x="33.76" y="54.06" width="87.36" height="31.09"/>
							    <rect id="_Rectangle_2" data-name="&lt;Rectangle&gt;" class="cls-22" x="34.42" y="85.15" width="87.36" height="20.05"/>
							    <rect id="_Rectangle_3" data-name="&lt;Rectangle&gt;" class="cls-32" x="26.74" y="74.27" width="14.19" height="31.12"/>
							    <rect id="_Rectangle_4" data-name="&lt;Rectangle&gt;" class="cls-32" x="114.02" y="74.27" width="14.19" height="31.12"/>
							  </g>
							  <rect id="puff" data-name="&lt;Rectangle&gt;" class="cls-22" x="84" y="65.75" width="19.05" height="19.05"/>
							  <g id="lamp" data-name="&lt;Group&gt;">
							    <line id="_Path_" data-name="&lt;Path&gt;" class="cls-42" x1="160.5" y1="105" x2="160.5" y2="44"/>
							    <polygon id="_Path_2" data-name="&lt;Path&gt;" class="cls-22" points="151.37 20.5 168.08 20.5 174 43.5 147.19 43.5 151.37 20.5"/>
							    <line id="_Path_3" data-name="&lt;Path&gt;" class="cls-22" x1="152" y1="104.5" x2="171" y2="104.5"/>
							  </g>
							</svg>
							<a class="red-link">Подбор и&nbsp;комплектация</a>
							<div class="service-text">
							Мы&nbsp;подбираем отделочные материалы и&nbsp;мебель по&nbsp;обозначенному бюджету, взаимодействуем с&nbsp;западными и&nbsp;российскими поставщиками и&nbsp;производителями, создаем технические задания для изготовления мебели на&nbsp;заказ, контролируем поставку.
							</div>
						</div>	
					</div>
					<div class="col-xs-12 col-sm-4">
						<div class="service-icon">
							<svg width="180" height="140" id="decor" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 180 140">
							  <defs>
							    <style>
							      .cls-13, .cls-23 {
							        fill: #fff;
							      }
							
							      .cls-23, .cls-33, .cls-43 {
							        stroke: #000;
							      }
							
							      .cls-23, .cls-43 {
							        stroke-linecap: round;
							        stroke-linejoin: round;
							        stroke-width: 3px;
							      }
							
							      .cls-33, .cls-43 {
							        fill: none;
							      }
							
							      .cls-33 {
							        stroke-miterlimit: 10;
							        stroke-width: 2px;
							      }
							    </style>
							  </defs>
							  <rect class="cls-13" width="180" height="140"/>
							  <g id="big-picture" data-name="&lt;Group&gt;">
							    <rect id="_Rectangle_" data-name="&lt;Rectangle&gt;" class="cls-23" x="25.26" y="40.92" width="55.21" height="72.2" transform="translate(-24.16 129.89) rotate(-90)"/>
							    <circle id="_Path_" data-name="&lt;Path&gt;" class="cls-33" cx="70.45" cy="63.85" r="5.91"/>
							    <path id="_Path_2" data-name="&lt;Path&gt;" class="cls-23" d="M45.95,88.23s25.55-7,42.55-4.63v21C67.5,90.28,16.77,82,16.77,82"/>
							  </g>
							  <g id="small-picture" data-name="&lt;Group&gt;">
							    <rect id="_Rectangle_2" data-name="&lt;Rectangle&gt;" class="cls-43" x="122.82" y="9.49" width="43.81" height="41.55" transform="translate(114.45 174.99) rotate(-90)"/>
							    <path id="_Path_3" data-name="&lt;Path&gt;" class="cls-43" d="M164,28.5"/>
							    <path id="_Path_4" data-name="&lt;Path&gt;" class="cls-43" d="M136.46,28.5"/>
							    <polyline id="_Path_5" data-name="&lt;Path&gt;" class="cls-43" points="123.94 31.49 133.47 23.71 152.75 52.17"/>
							    <polyline id="_Path_6" data-name="&lt;Path&gt;" class="cls-43" points="144.72 39.35 155.11 31.79 165.5 41.05"/>
							  </g>
							</svg>
							<a class="red-link">Декорирование</a>
							<div class="service-text">
							Мы&nbsp;умеем сделать интерьер живым и&nbsp;законченным: подобрать аксессуары, текстиль и&nbsp;художественные произведения, создать авторские предметы интерьера, вдохнуть новую жизнь в&nbsp;уже имеющиеся у&nbsp;вас вещи. Также мы&nbsp;делаем художественную роспись стен и&nbsp;потолков.
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-4">
						<div class="service-icon">
							<svg id="audit" width="140" height="140" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 140 140">
							  <defs>
							    <style>
							      .cls-14 {
							        fill: #fff;
							      }
							
							      .cls-24 {
							        fill: none;
							        stroke: #000;
							        stroke-linecap: round;
							        stroke-linejoin: round;
							        stroke-width: 3px;
							      }
							    </style>
							  </defs>
							  <rect class="cls-14" width="140" height="140"/>
							  <g id="house" data-name="&lt;Group&gt;">
							    <polygon id="_Path_" data-name="&lt;Path&gt;" class="cls-24" points="38.66 53.24 59.84 37.88 81.03 54.5 75.5 54.5 75.5 72.5 43.5 72.5 43.5 53.28 38.66 53.24"/>
    							<rect id="_Rectangle_" data-name="&lt;Rectangle&gt;" class="cls-24" x="53.26" y="62.28" width="13.67" height="10.49"/>
    							<polygon id="_Path_2" data-name="&lt;Path&gt;" class="cls-24" points="60.09 45.72 55.27 50.55 64.92 50.55 60.09 45.72"/>
							  </g>
							  <g id="_Group_2" data-name="&lt;Group&gt;">
							    <path id="_Path_3" data-name="&lt;Path&gt;" class="cls-24" d="M26.17,63.17A38.72,38.72,0,1,0,64.89,24.45,38.72,38.72,0,0,0,26.17,63.17Z"/>
							    <polyline id="_Path_4" data-name="&lt;Path&gt;" class="cls-24" points="93.1 92.57 115.71 115.33 120.06 110.98 97.17 88.09"/>
							  </g>
							</svg>
							<a class="red-link">Архитектурное проектирование</a>
							<div class="service-text">
							Мы&nbsp;проектируем индивидуальные жилые загородные дома, прилегающую территорию, а&nbsp;также бани, гаражи и&nbsp;гостевые домики, чтобы комплесно решить задачу заказчиков по&nbsp;обустройству и&nbsp;грамотному использованию пространства участка.
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4">
						<div class="service-icon">
							<svg id="ruler-pen2" width="160" height="140" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 160 140">
							  <defs>
							    <style>
							      .cls-15, .cls-25 {
							        fill: #fff;
							      }
							
							      .cls-25, .cls-45 {
							        stroke: #000;
							        stroke-linecap: round;
							        stroke-linejoin: round;
							        stroke-width: 3px;
							      }
							
							      .cls-35, .cls-45 {
							        fill: none;
							      }
							    </style>
							  </defs>
							  <rect class="cls-15" width="160" height="140"/>
							  <g id="pen2">
							    <g data-name="&lt;Group&gt;">
							      <g id="_Group_4" data-name="&lt;Group&gt;">
							        <path id="_Path_10" data-name="&lt;Path&gt;" class="cls-35" d="M107.37,18.31l0.43-5.12a1.74,1.74,0,0,1,3.16-.85l2.93,4.22"/>
							      </g>
							      <g id="_Group_5" data-name="&lt;Group&gt;">
							        <polyline id="_Path_11" data-name="&lt;Path&gt;" class="cls-15" points="120.94 30.58 140.17 103.08 127.86 106.38 108.75 34.3"/>
							        <path id="_Path_12" data-name="&lt;Path&gt;" class="cls-45" d="M120.55,29.14l-12.31,3.3Z"/>
							        <polygon id="_Path_13" data-name="&lt;Path&gt;" class="cls-45" points="107.77 33.79 121.78 86.09 129.97 116.68 143.5 113.06 121.29 30.17 109.53 13.34 107.77 33.79"/>
							      </g>
							    </g>
							    <path id="_Path_14" data-name="&lt;Path&gt;" class="cls-45" d="M127.73,105.89L140,102.6Z"/>
							    <path id="_Path_15" data-name="&lt;Path&gt;" class="cls-25" d="M124.21,68.34"/>
							  </g>
							  <g id="ruler" data-name="&lt;Group&gt;">
							    <rect id="_Rectangle_" data-name="&lt;Rectangle&gt;" class="cls-25" x="43.9" y="15.9" width="18.87" height="100.14" transform="translate(40.13 -17.83) rotate(30)"/>
							    <g id="_Group_2" data-name="&lt;Group&gt;">
							      <line id="_Path_" data-name="&lt;Path&gt;" class="cls-25" x1="66.49" y1="24.52" x2="71.68" y2="27.52"/>
							      <line id="_Path_2" data-name="&lt;Path&gt;" class="cls-25" x1="60.99" y1="34.05" x2="67.91" y2="38.05"/>
							      <line id="_Path_3" data-name="&lt;Path&gt;" class="cls-25" x1="56.49" y1="41.84" x2="61.68" y2="44.84"/>
							      <line id="_Path_4" data-name="&lt;Path&gt;" class="cls-25" x1="50.49" y1="52.23" x2="57.41" y2="56.23"/>
							      <line id="_Path_5" data-name="&lt;Path&gt;" class="cls-25" x1="44.99" y1="61.76" x2="50.18" y2="64.76"/>
							      <line id="_Path_6" data-name="&lt;Path&gt;" class="cls-25" x1="39.99" y1="70.42" x2="45.18" y2="73.42"/>
							      <line id="_Path_7" data-name="&lt;Path&gt;" class="cls-25" x1="35.49" y1="78.22" x2="42.41" y2="82.22"/>
							      <line id="_Path_8" data-name="&lt;Path&gt;" class="cls-25" x1="29.99" y1="87.74" x2="35.18" y2="90.74"/>
							      <line id="_Path_9" data-name="&lt;Path&gt;" class="cls-25" x1="24.99" y1="96.4" x2="31.91" y2="100.4"/>
							    </g>
							  </g>
							</svg>
							<a class="red-link">Управление проектом</a>
							<div class="service-text">
							Мы&nbsp;берем на&nbsp;себя все заботы, связанные с&nbsp;реализацией проекта: рекомендуем строительных подрядчиков, находим подрядчиков по&nbsp;инженерным решениям, ведём авторский надзор и&nbsp;занимаемся комплектацией объекта.
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4">
						<div class="service-icon">
							<svg id="brand" xmlns="http://www.w3.org/2000/svg" width="140" height="140" viewBox="0 0 140 140">
							  <defs>
							    <style>
							      .cls-41 {
							        fill: #fff;
							      }
							
							      .cls-42 {
							        fill: none;
							        stroke: #000;
							        stroke-miterlimit: 10;
							        stroke-width: 3px;
							      }
							    </style>
							  </defs>
							  <rect class="cls-41" width="140" height="140"/>
							  <polygon id="_Path_2" data-name=" Path 2" class="cls-42" points="121 113.89 121 70.34 101.33 70.34 101.33 26.79 40.67 26.79 40.67 49.89 20.98 49.89 20.98 113.89 121 113.89"/>
							  <line id="brand1" class="cls-42" x1="101.33" y1="70.34" x2="41" y2="70.34"/>
							  <line id="brand2" class="cls-42" x1="77" y1="102.51" x2="77" y2="70.34"/>
							  <line id="brand3" class="cls-42" x1="41" y1="82.34" x2="77" y2="82.34"/>
							</svg>
							<a class="red-link">Согласование перепланировки</a>
							<div class="service-text">
							Мы&nbsp;консультируем заказчиков по&nbsp;поводу существующих норм и&nbsp;требований, разрабатываем проект перепланировки и&nbsp;техническое заключение для согласования в&nbsp;Мосжилинспекции и&nbsp;управляющих компаниях, наш специалист по&nbsp;согласованию перепланировок получает все разрешения и&nbsp;новые планы БТИ после завершения работ.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
