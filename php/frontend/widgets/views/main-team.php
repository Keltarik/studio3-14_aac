<?php

/* @var $this \yii\web\View */
/* @var $model array */

$attachment = new \common\models\type\Teammate();

$totalCount = count($model);
$countFirstBlock = $totalCount > 6 ? 6 : $totalCount;

?>

<?php if (count($model) > 0): ?>

    <div class="team__persons-block">
        <div class="team__persons">
            <?php for ($i = 0; $i < $countFirstBlock; $i++ ): ?>
                <?php $attachment->photo = $model[$i]['photo'] ?>
                <div class="team__person">
                    <img src="<?= Yii::getAlias('@web') . $attachment->getPhotoUrl() ?>" alt="<?= $model[$i]['name'] ?>" class="team__person-image">
                    <p class="team__person-name">
                        <?= $model[$i]['name'] ?>
                    </p>
                    <p class="team__person-description">
                        <?= $model[$i]['profession'] ?>
                    </p>
                </div>
            <?php endfor; ?>
        </div>
        <div class="team__person-comment">
            <p class="team__person-comment-title">
                “У меня есть для<br>
                вас интересное<br>
                предложение”
            </p>

            <div class="team__person-comment-image">
                <img src="<?= Yii::getAlias('@web') ?>/img/person__video.jpg" alt="">
                <a class="team__person-comment-play" href="https://www.youtube.com/watch?v=8IiVqwpQLYw" data-fancybox="">
                    <img src="<?= Yii::getAlias('@web') ?>/img/icons/play.png" alt="">
                    <span>Смотреть<br>видео</span>
                </a>
            </div>

            <a href="javascript:;" class="btn team__person-comment-btn open-modal">
                <span>получить</span>
                <i class="icon"><svg><use xlink:href="#icon_gift"></use></svg></i>
            </a>
        </div>
    </div>
    <div class="team__persons team__persons-long">
        <?php if ($totalCount > 6): ?>
            <?php for ($i = 6; $i < $totalCount; $i++): ?>
                <?php $attachment->photo = $model[$i]['photo'] ?>
                <div class="team__person">
                    <img src="<?= Yii::getAlias('@web') . $attachment->getPhotoUrl() ?>" alt="<?= $model[$i]['name'] ?>" class="team__person-image">
                    <p class="team__person-name">
                        <?= $model[$i]['name'] ?>
                    </p>
                    <p class="team__person-description">
                        <?= $model[$i]['profession'] ?>
                    </p>
                </div>
            <?php endfor; ?>
        <?php endif; ?>
    </div>

<?php endif; ?>