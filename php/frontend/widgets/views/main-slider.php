<?php

/* @var $this \yii\web\View */
/* @var $model \common\models\type\Slider */

?>

<?php if ($model): ?>
<div class="owl-carousel">
    <?php foreach ($model as $slide): ?>
        <div><img src="<?= Yii::getAlias('@web') ?>/upload/slider/<?= $slide->picture ?>" alt="<?= $slide->text ?>"></div>
    <?php endforeach; ?>
</div>
<?php endif; ?>
