<?php

namespace frontend\controllers;


use frontend\models\form\AddReport;

use frontend\models\form\CallbackForm;
use frontend\models\form\UploadFileCallback;
use frontend\models\type\Project;


use frontend\models\type\Teammate;
use frontend\models\type\Partner;
use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\Controller;
use yii\web\UploadedFile;


class SiteController extends Controller {

	public $social_title = '';
	public $social_image = '';
	public $social_description = '';
    public $social_image_vk = '';

	public $defaultAction = 'index';

	private function __setSocialMetaFromArray($arr) {
		$this->__setSocialMeta($arr[0], $arr[1], $arr[2]);
	}

	private function __setSocialMeta($title, $img, $description) {
		\Yii::$app->view->registerMetaTag(['property' => 'og:title', 'content' => $title], "social_title");
		\Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => $img], "social_image");
		\Yii::$app->view->registerMetaTag(['property' => 'og:description', 'content' => $description], "social_description");
        \Yii::$app->view->registerLinkTag(['rel' => 'image_src', 'href' => $img], "social_image_vk");
	}

	public function actionIndex() {
		$this->__setSocialMeta(
			"«Студия 3.14» — стильный и практичный дизайн интерьера в Москве и Подмосковье",
			'http://studio3-14.ru/img/slider/3.jpg',
			'Мы команда единомышленников — дизайнеров и архитекторов. Наша работа — создавать пространства для жизни.'
		);

		$callbackModel = new AddReport();

		return $this->render('index', [
		    'callbackModel' => $callbackModel
        ]);
	}


	public function actionProject($slug) {
		/**
		 * @var $project Project
		 */

		$this->layout = 'inner';

		$project = Project::findOne(['slug' => $slug]);
		if (!$project)
			throw new \yii\web\HttpException(404, 'The requested Item could not be found.');

		$this->__setSocialMetaFromArray($project->getSocial());

		return $this->render('project', ['project' => $project]);
	}


	public function actionAbout() {
	    $this->layout = 'inner';
		return $this->render('about', []);
	}

    public function actionThankyou() {
        $this->layout = 'inner';
        return $this->render('thankyou', []);
    }
	
	public function actionPartners() {
	    $this->layout = 'inner';
		return $this->render('partners', []);
	}

    public function actionPortfolio() {
        $this->layout = 'inner';
        return $this->render('portfolio', []);
    }

    public function actionPartner($id) {
        $this->layout = 'inner';
        return $this->renderFile('@app/views/low/partner.php', ['partner' => Partner::findOne(['id' => $id])]);
    }

	public function actionPerson($id) {
        $this->layout = 'inner';
		return $this->renderFile('@app/views/low/person.php', ['person' => Teammate::findOne(['id' => $id])]);
	}

	public function actionCallback()
    {
        if (Yii::$app->request->isPost) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $data = Yii::$app->request->post();

            $message = \Yii::$app->mailer->compose(
                "calculator",
                [
                    'project__designe' => isset($data['project__designe']) ? $data['project__designe'] : '',
                    'project__area_width' => isset($data['project__area_width']) ? $data['project__area_width'] : '',
                    'project__home' => isset($data['project__home']) ? $data['project__home'] : '',
                    'project__new_home' => isset($data['project__new_home']) ? $data['project__new_home'] : '',
                    //'project__file' => $data['project__file'],
                    'project__male_female' => isset($data['project__male_female']) ? $data['project__male_female'] : '',
                    'project__needed' => isset($data['project__needed']) ? $data['project__needed'] : '',
                    'project__user_style' => isset($data['project__user_style']) ? $data['project__user_style'] : '',
                    'project__user_name' => isset($data['project__user_name']) ? $data['project__user_name'] : '',
                    'project__user_phone' => isset($data['project__user_phone']) ? $data['project__user_phone'] : '',
                    'project__user_email' => isset($data['project__user_email']) ? $data['project__user_email'] : ''
                ]
            )->setFrom(\Yii::$app->params['adminEmail'])
                ->setTo(\Yii::$app->params['reportEmail'])
                ->setSubject('Новая заявка на сайте studio 314');


            $modelFile = new UploadFileCallback();
            $modelFile->load(Yii::$app->request->post());

            $file = UploadedFile::getInstance($modelFile, 'file');

            if($file) {
                $filename = Yii::getAlias('@uploadsroot') . '/mail/' . $file->baseName . '.' . $file->extension;

                if ($file->saveAs($filename)) {
                    $message->attach($filename);
                }
            }

            $message->send();

            return ['type' => 'success', 'message' => 'Сообщение успешно отправлено!'];
        }

        throw new NotFoundHttpException('Страница не найдена');
    }

	public function actionConsult() {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$model = new AddReport();

		if ($model->go()) {
            $this->__sendAdminReport($model);
			return array(
				'error' => false, 'success' => true, 'message' => 'Сообщение успешно отправлено!'
			);
		}

		$errorData = $model->getFirstErrors();

		return array(
			'error' => true, 'success' => true, 'message' => $errorData
		);
	}

	private function __sendAdminReport($model) {

		\Yii::$app->mailer->compose(
			"mail",
			[
				'user_name' => $model->user_name,
				'user_phone' => $model->user_phone,
				'user_email' => $model->user_email,
				//'user_text' => $model->user_text
			]
		)->setFrom(\Yii::$app->params['adminEmail'])
			->setTo(\Yii::$app->params['reportEmail'])
			->setSubject('Новая заявка на сайте studio 314')
			->send();
	}

	public function actionError() {
        $this->layout = 'inner';
		$exception = Yii::$app->errorHandler->exception;
		return $this->render('error', ['exception', $exception]);
	}

	/**
	 *
	 */
	public function actionTest() {


		print_r(Url::to('@web'));

//		\Yii::$app->mailer->compose(
//			"mail",
//			[
//				'user_name' => 'TEST',
//				'user_phone' => '8-987-654-32-10',
//				'user_email' => 'test@test.com',
//				'user_text' => 'Тестовый текст'
//			]
//		)->setFrom(\Yii::$app->params['adminEmail'])
//			->setTo(\Yii::$app->params['reportEmail'])
//			->setSubject('Проверка почты')
//			->send();

	}
}
