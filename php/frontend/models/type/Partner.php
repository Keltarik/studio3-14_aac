<?php

namespace frontend\models\type;


class Partner extends \common\models\type\Partner {
	/**
	 * @return Project[]
	 */
	function getProjects() {
		return Project::find()
			->where('FIND_IN_SET(' . $this->getAttribute('id') . ',`partner-ids`)')
			->all();
	}
}