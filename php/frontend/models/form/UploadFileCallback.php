<?php
/**
 * Created by PhpStorm.
 * User: macos
 * Date: 10.10.17
 * Time: 8:11
 */

namespace frontend\models\form;


use yii\base\Model;

class UploadFileCallback extends Model
{
    public $file;

    public function rules()
    {
        return [
            [['file'], 'file']
        ];
    }
}