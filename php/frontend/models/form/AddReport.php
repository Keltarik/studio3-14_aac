<?php
/**
 * Created by PhpStorm.
 * User: zachot
 * Date: 05.08.15
 * Time: 17:09
 */

namespace frontend\models\form;


use frontend\models\Report;
use yii\base\Model;

class AddReport extends Model {

	public $user_name;
	public $user_phone;
	public $user_email;
	public $user_text;

	public $project_id;

	/**
	 * @return array
	 */
	public function rules() {
		return [
			[['user_name', 'user_phone', 'user_email', 'user_text'], 'string'],
			[['project_id'], 'number'],
			[['user_email'], 'email'],
			[['user_phone', 'user_email'], 'checkIsEmpty'],
		];
	}

	public function checkIsEmpty() {
		if (empty($this->user_phone)) {
			$this->addError('save', 'Надо заполнить поле телефона!');
		}
        if (strlen($this->user_phone) < 9) {
            $this->addError('save', 'Пожалуйста, введите номер телефона!');
        }
	}

	public function save() {
		$report = new Report();

		$report->setAttribute('project_id', $this->project_id);
		$report->setAttribute('user_name', $this->user_name);
		$report->setAttribute('user_phone', $this->user_phone);
		$report->setAttribute('user_email', $this->user_email);
		$report->setAttribute('user_text', $this->user_text);

		date_default_timezone_set('UTC');

		$report->setAttribute('datetime', date('Y-m-d H:i:s', time()));

        if (empty($this->user_email)) {
            $report->save();
        }

//		$this->addErrors($report->errors);

//		return !$this->hasErrors();
        return true;
	}


	public function go() {
		$saving = 
			$this->load(\Yii::$app->request->post())
			&& $this->validate()
			&& $this->save();
			
		if($saving && isset($_COOKIE['roistat_visit'])) {
			$roistatData = array(
				'roistat' => isset($_COOKIE['roistat_visit']) ? $_COOKIE['roistat_visit'] : null,
				'key'     => 'NTg3MDc6NTQ2OTU6ZDM2NDNjNTBlNWZhZTcxYzIzM2Y5MzU3YmI5ZGQyMDg=',
				'title'   => 'Новый лид с сайта studio3-14.ru',
				'name'    => !empty($this->user_name) ? $this->user_name : 'Новый лид с сайта',
				'email'   => !empty($this->user_email) ? $this->user_email : null,
				'phone'   => !empty($this->user_phone) ? $this->user_phone : null,
				'comment' => !empty($this->user_text) ? $this->user_text : null,
				'fields'  => array(
				'SOURCE_ID' => 'WEB',
				'SOURCE_DESCRIPTION' => "{source}",
				'utm' => "{utmSource} {utmMedium} {utmCampaign} {utmTerm} {utmContent}"
				),
			);
			file_get_contents("https://cloud.roistat.com/api/proxy/1.0/leads/add?" . http_build_query($roistatData));
		}
		
		return $saving;
	}
}