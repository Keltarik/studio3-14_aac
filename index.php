<?php
//phpinfo();
//die();
error_reporting(E_ALL);
ini_set('display_errors','On');

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', true);

require(__DIR__ . '/php/vendor/autoload.php');
require(__DIR__ . '/php/vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/php/common/config/bootstrap.php');
//require(__DIR__ . '../php/frontend/config/bootstrap.php');

$config = yii\helpers\ArrayHelper::merge(
	require(__DIR__ . '/php/common/config/main.php'),
	require(__DIR__ . '/php/frontend/config/main.php')
);

$application = new yii\web\Application($config);
$application->run();



